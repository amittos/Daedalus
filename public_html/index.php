﻿<?php
session_start();

if(isset($_SESSION['user_role'])){
	header( 'Location: authentication.php' ) ;
}

$error="";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="el" xml:lang="en"> 

<head>
	<title> Δαίδαλος </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Language" content="el">

	<!--<link rel="stylesheet" href="styles/basic/input.css" type="text/css" media="screen"/>--> <!-- Αρχικό CSS - Υλοποίηση Κυριάκου -->
	<link rel="stylesheet" href="styles/basic/style.css"> <!-- New CSS of Doom for the Header -->
	<link rel="shortcut icon" href="https://pithos.grnet.gr/pithos/rest/icsd08158@aegean.gr/files/favicon.icon" />
	
	<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">--> <!-- Αυθεντική βιβλιοθήκη JQuery στην οποία βασιστήκαμε -->
	<link rel="stylesheet" href="styles/basic/jquery-ui.css"> <!-- JQuery βιβλιοθήκη, με πειραγμένα τα Tabs -->
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<!-- FullCalendar stylesheet and FullCalendar JS file -->
	<link rel='stylesheet' type='text/css' href='fullcalendar.css' />
	<script type='text/javascript' src='fullcalendar.js'></script>
	
	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="jquery.dataTables.css">  <!-- Local Installation -->
	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
</head>
<script>
	$(function() {
		   	var tooltips = $( "[title]" ).tooltip({
		     	position: {
		        	my: "left top",
		        	at: "right+6 top-5"
		     	}
		    });
	    });
</script>
<style type="text/css">
	
	.ui-tooltip {
   			 padding-bottom: 0px;
   			 padding-top: 0px;
   			 max-width: 200px;
   			 background: rgb(250,250,250);
   			 font-size: 11px;
   			 border: 10px;
   			 line-height: 15px;
  		}

</style>

<body>
	<link rel="stylesheet" href="styles/basic/input.css" type="text/css" media="screen"/>
	<center>
		<table class="wrapper">
			<tbody>
				<tr>
					<td valign="top">
						<center>
							<table width="90%">
								<tr>
									<td align="center" valign="top">
									<td align="center" valign="top">
										
										<?php
											require_once("includes/header_login2.php");
										?>

										<?php
											if(isset($_GET['error'])){
											$error=$_GET['error'];
												if($error == "not_found"){
													echo "<br><font color=\"red\">O συνδυασμός username και password που καταχωρήσατε δεν είναι έγκυρος<br></font>";
												}
											}	
										?>

										<?php
											if(isset($_GET['status']) && $_GET['status']=="success"){
												echo  "<br><h4><font style=\" color:#696969;\"> Επιτυχία σύνδεσης!</font></h3>";
											}
										?>

										<br><br>
										<br><br>
										<div id="component_login">
											<table width="40%">
												<form action="authentication.php" method="post" >
													<tbody>
														<tr>
															<td align="left">Username:</td>
															<td><input title="Για την είσοδο χρησιμοποιείστε το username και το password του Πανεπιστημίου Αιγαίου" type="text" name="username"></td>
														</tr>
														<tr>
															<td>Password:</td>
															<td><input type="password" name="password"></td>
														</tr>
														<tr>
															<td colspan="2"><input name="submit" type="submit" value="Login"></td>
														</tr>
													</tbody>	      
												</form>		
											</table>
										</div>
									</td>
								</tr>
							</table>
							
							<br><br><br><br><br><br>

							<?php
								require_once("includes/footer.php");
							?>

						</center>
					</td>
				</tr>
			</tbody>
		</table>
	</center>
</body>

