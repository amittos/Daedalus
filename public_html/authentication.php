﻿<?php
session_start(); // Open Session
//require_once("../dtbs.php");
require_once("ldap_connect.php");
// require_once("includes/functions.php");

if (isset($_POST['username']))
	$username =  htmlspecialchars($_POST['username']);
else
	$username = "";

if (isset($_POST['password']))
	$password = $_POST['password'];
else 
	$password = "";
if (isset($username) && (isset($password))) { // if username & password not null

	$r2 = @ldap_bind($ds, "aegean\\$username", $password); //connect ($ds) and bind to ldap server using the username & password given

	if (!$r2){
		header("Location: index.php?msg=auth_ldap_er"); // in case of an error display an authentication error on index.php
		exit();
	}

	$sr = ldap_search($ds, $ldap_dn, "samaccountname=$username"); //search (connect to ldap, to the aegean.gr domain, search for the username given)
	$ldap_entries = ldap_count_entries($ds, $sr); // count and return the number of entries stored in the result of $sr search operation.

	if ($ldap_entries > 0) { //if the username is found in the ldap server
		$info = ldap_get_entries($ds, $sr); //Read multiple entries from the given result, and then reading the attributes and multiple values
		$ldap_entries = ldap_count_entries($ds, $sr); // count and return the number of entries stored in the result of $sr search operation
		$_SESSION['username'] = $info[0]["samaccountname"][0]; //insert the value of the username into the Session's username variable 
		$_SESSION['fname'] = $info[0]["givenname"][0]; //insert the value of the First name into the Session's ufirst name variable 
		$_SESSION['lname'] = $info[0]["sn"][0]; //insert the value of the the Last name  into the Session's last name variable 
		$_SESSION['email'] = $info[0]["mail"][0]; //insert the value of the email into the Session's email variable 
		$_SESSION['department'] = $info[0]["department"][0]; //insert the value of the department into the Session's email variable 
		

		ldap_close($ds);
		$_SESSION['username'] = $username; //session according to the username
		header("Location:fullcalendar.php"); //go to the page form_lab.php
		exit();
	}
	else{									//if the username not found on the ldap
		ldap_close($ds);
		header("Location: index.php?msg=auth_ldap_er"); //go to index.php with an error message
		exit();
	}					 			  
	
}
else {
		header("Location: index.php?msg=required_er"); //if username and password fields are null go to index.php and display a message
		exit();
	}
	?>
