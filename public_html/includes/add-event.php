<?php

	//include("database.php");
	if(isset($_POST['ID-Anathesis'])){
    	$title = $_POST['title']; 
        $aithousa = $_POST['aithousa'];
        $aithousaID = $_POST['ID-Aithousas'];
    	$start_date = $_POST['event-date'];
        $weekday = date('N', strtotime($start_date));
        $start_time = $_POST['start-time'];
        $end_time = $_POST['end-time'];
        $anathesiID = $_POST['ID-Anathesis'];
        $start = strtotime($start_time);
        $end = strtotime($end_time);
        $repeat_freq = $_POST['repeat-freq'];
	}
    else if(isset($_POST['ID-Anathesis-ena'])){
        $title = $_POST['title-ena']; 
        $aithousa = $_POST['aithousa-ena'];
        $aithousaID = $_POST['ID-Aithousas-ena'];
        $start_date = $_POST['event-date-ena'];
        $weekday = date('N', strtotime($start_date));
        $start_time = $_POST['start-time-ena'];
        $end_time = $_POST['end-time-ena'];
        $anathesiID = $_POST['ID-Anathesis-ena'];
        $start = strtotime($start_time);
        $end = strtotime($end_time);
        $repeat_freq = $_POST['repeat-freq'];
    }
    else if(isset($_POST['ID-Anathesis-dio'])){
        $title = $_POST['title-dio']; 
        $aithousa = $_POST['aithousa-dio'];
        $aithousaID = $_POST['ID-Aithousas-dio'];
        $start_date = $_POST['event-date-dio'];
        $weekday = date('N', strtotime($start_date));
        $start_time = $_POST['start-time-dio'];
        $end_time = $_POST['end-time-dio'];
        $anathesiID = $_POST['ID-Anathesis-dio'];
        $start = strtotime($start_time);
        $end = strtotime($end_time);
        $repeat_freq = $_POST['repeat-freq'];
    }
    else if(isset($_POST['ID-Anathesis-tria'])){
        $title = $_POST['title-tria']; 
        $aithousa = $_POST['aithousa-tria'];
        $aithousaID = $_POST['ID-Aithousas-tria'];
        $start_date = $_POST['event-date-tria'];
        $weekday = date('N', strtotime($start_date));
        $start_time = $_POST['start-time-tria'];
        $end_time = $_POST['end-time-tria'];
        $anathesiID = $_POST['ID-Anathesis-tria'];
        $start = strtotime($start_time);
        $end = strtotime($end_time);
        $repeat_freq = $_POST['repeat-freq'];
    }
    else if(isset($_POST['ID-Anathesis-tessera'])){
        $title = $_POST['title-tessera']; 
        $aithousa = $_POST['aithousa-tessera'];
        $aithousaID = $_POST['ID-Aithousas-tessera'];
        $start_date = $_POST['event-date-tessera'];
        $weekday = date('N', strtotime($start_date));
        $start_time = $_POST['start-time-tessera'];
        $end_time = $_POST['end-time-tessera'];
        $anathesiID = $_POST['ID-Anathesis-tessera'];
        $start = strtotime($start_time);
        $end = strtotime($end_time);
        $repeat_freq = $_POST['repeat-freq'];
    }
    else if(isset($_POST['ID-Anathesis-pente'])){
        $title = $_POST['title-pente']; 
        $aithousa = $_POST['aithousa-pente'];
        $aithousaID = $_POST['ID-Aithousas-pente'];
        $start_date = $_POST['event-date-pente'];
        $weekday = date('N', strtotime($start_date));
        $start_time = $_POST['start-time-pente'];
        $end_time = $_POST['end-time-pente'];
        $anathesiID = $_POST['ID-Anathesis-pente'];
        $start = strtotime($start_time);
        $end = strtotime($end_time);
        $repeat_freq = $_POST['repeat-freq'];
    }
	$difference = $end - $start;
	$h_difference = date('h', $difference) - 2;
    $final_title = $title." - ".$aithousa;
    
        $repeats = 1;
        $until = (365/$repeat_freq);
        if ($repeat_freq == 1){
            $weekday = 0;
        }
        $dbh = new PDO("mysql:host=127.0.0.1; dbname=daedalus;", 'daedalus', '!daedalus!'); //Δεν είναι ασφαλές
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);  // set the error mode to excptions
		$dbh->exec("SET NAMES 'utf8';");
        $dbh->beginTransaction();
        try{
            $stmt = $dbh->prepare("INSERT INTO Event_Parent 
                (title,start_date, start_time, end_time, weekday, repeats, repeat_freq, ID_Anathesis, ID_Aithousas)
                VALUES (:title, :start_date, :start_time, :end_time, :weekday, :repeats, :repeat_freq, :ID_Anathesis, :ID_Aithousas)");

            $stmt->bindParam(':title', $final_title);
            $stmt->bindParam(':start_date', $start_date);
            $stmt->bindParam(':start_time', $start_time);
            $stmt->bindParam(':end_time', $end_time);
            $stmt->bindParam(':weekday', $weekday);
            $stmt->bindParam(':repeats', $repeats);
            $stmt->bindParam(':repeat_freq', $repeat_freq);
            $stmt->bindParam(':ID_Anathesis', $anathesiID);
            $stmt->bindParam(':ID_Aithousas', $aithousaID);
            $stmt->execute();
            $last_id = $dbh->lastInsertId();

            for($x = 0; $x <$until; $x++){
                $stmt = $dbh->prepare("INSERT INTO Event 
                    (title, start_date, start_time, end_time, duration, parent_id)
                    VALUES (:title, :start_date, :start_time, :end_time, :duration, :parent_id)");
                $stmt->bindParam(':title', $final_title );
                $stmt->bindParam(':start_date', $start_date);
                $stmt->bindParam(':start_time', $start_time);
                $stmt->bindParam(':end_time', $end_time);
                $stmt->bindParam(':duration', $h_difference);
                $stmt->bindParam(':parent_id', $last_id);
                $stmt->execute();
                $start_date = date('Y-m-d', strtotime($start_date .'+'. $repeat_freq .'days'));
                //$end_date = strtotime($end . '+' . $repeat_freq . 'DAYS');
                //$start = date("Y-m-d H:i", $start_date);
                //$end = date("Y-m-d H:i", $end_date);
            }
            $dbh->commit();
        }

        catch(Exception $e){
            $dbh->rollback();
        }

   echo "Done";
	
?>
