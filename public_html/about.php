<?php
session_start();

if(isset($_SESSION['user_role'])){
	header( 'Location: authentication.php' ) ;
}

$error="";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="el" xml:lang="en"> 

<head>
	<title> Δαίδαλος </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Language" content="el">

	<!--<link rel="stylesheet" href="styles/basic/input.css" type="text/css" media="screen"/>--> <!-- Αρχικό CSS - Υλοποίηση Κυριάκου -->
	<link rel="stylesheet" href="styles/basic/style.css"> <!-- New CSS of Doom for the Header -->
	<link rel="shortcut icon" href="https://pithos.grnet.gr/pithos/rest/icsd08158@aegean.gr/files/favicon.icon" />
	
	<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">--> <!-- Αυθεντική βιβλιοθήκη JQuery στην οποία βασιστήκαμε -->
	<link rel="stylesheet" href="styles/basic/jquery-ui.css"> <!-- JQuery βιβλιοθήκη, με πειραγμένα τα Tabs -->
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<!-- FullCalendar stylesheet and FullCalendar JS file -->
	<link rel='stylesheet' type='text/css' href='fullcalendar.css' />
	<script type='text/javascript' src='fullcalendar.js'></script>
	
	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="jquery.dataTables.css">  <!-- Local Installation -->
	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
</head>

<body>
	<link rel="stylesheet" href="styles/basic/input.css" type="text/css" media="screen"/>
	<center>
		<table class="wrapper">
			<tbody>
				<tr>
					<td valign="top">
						<center>
							<table width="50%">
								<tr>
									<td  valign="top">
										
										<?php
											require_once("includes/header_login2.php");
										?>
										<br><br><br><br>
										<font class="about">
											Ο Δαίδαλος αποτελεί την Διπλωματική εργασία των Αλέξανδρου Μήττου και Κυριάκου Χατζηκυριάκου, φοιτητών του τμήματος Μηχανικών Πληροφοριακών και Επικοινωνιακών Συστημάτων του Πανεπιστημίου Αιγαίου στη Σάμο. Ο Δαίδαλος είναι μία πρωτότυπη εφαρμογή δημιουργίας και διαχείρησης προγράμματος μαθημάτων των πανεπιστημιακών τμημάτων. 
										</font>
										
									</td>
								</tr>
							</table>
							
							<br><br><br>

							<?php
								require_once("includes/footer.php");
							?>

						</center>
					</td>
				</tr>
			</tbody>
		</table>
	</center>
</body>

