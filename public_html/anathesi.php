﻿<?php
	session_start();
	/*
	if($_SESSION['user_role'] != "2" && $_SESSION['user_role'] != "4"){
		header( 'Location: not_authorized.php' ) ;
	}
	*/
?>
<?php
	include ("database.php");
	
	$sql = "select ID_Didaskonta as value, eponimo as label, onoma as name from Didaskon_Orismos"; // Διάβασε Όνομα και ID μαθήματος από την βάση
	$result = mysql_query($sql); // Βάλ'τα στο result
	$data = array(); // Φτιάξε έναν πίνακα με όνομα data

	while ($row = mysql_fetch_object($result)) // Βάλε όλα τα στοιχεία στον πίνακα data
	{
	  $data[] = $row;
	}
	
	$sql2 = "select ID_OMathimatos as value, onoma as label from Mathima_Orismos"; // Διάβασε Όνομα και ID μαθήματος από την βάση
	$result2 = mysql_query($sql2); // Βάλ'τα στο result
	$data2 = array(); // Φτιάξε έναν πίνακα με όνομα data

	while ($row2 = mysql_fetch_object($result2)) // Βάλε όλα τα στοιχεία στον πίνακα data
	{
	  $data2[] = $row2;
	}	
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" lang="el" xml:lang="en"> 

<head>

	<!--
	//======================================================================//
	//																		//
	//								Libraries								//
	//																		//
	//======================================================================//
	-->

	<title> Ανάθεση Μαθημάτων - Δαίδαλος </title>
	
	<!-- JQuery Libray -->
	<script class="jsbin" src="http://code.jquery.com/jquery-1.11.0.js"></script>
	<script src="https://github.com/arshaw/fullcalendar/blob/master/src/loader.js"></script>
	<script src='jquery-ui.custom.min.js'></script>
	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/themes/ui-lightness/jquery-ui.css">
	<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="jquery.dataTables.css">  <!-- Local Installation -->
	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>

	<title> Daedalus </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Language" content="el">	
	
	<script>

		$(document).ready( function () { //Script του DataTable
		    $('#table_id').DataTable();
		} );

		$(function() {
			$("#typos" ).buttonset();
		});

		$(function() {
		   	var tooltips = $( "[title]" ).tooltip({
		     	position: {
		        	my: "left top",
		        	at: "right+6 top-1"
		     	}
		    });
	    });
		
		//Onclick για το delete button καθε αναθεσης. Η συνάρτηση παίρνει σαν παράμετρο το ID της ανάθεσης 
		function onClick(ID_Anathesis){
		
			$("#delete_anathesi_id" ).val(ID_Anathesis);
			$.ajax({
					type:"POST",
					url: "includes/delete-anathesi.php",
					data: $('#delete_mathima_form').serialize(),
					success: function(){
						
						location.reload();
						
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την διαγραφή. Παρακαλώ προσπαθήστε ξανά.");
					}
				});
		
		}
		
		//Κώδικας εισαγωγής μιας νεας ανάθεσης με τα στοιχεία της φόρμας
		$(function() {
			$( "input[type=submit]" )
				.button()
				.click(function( event ) {
				event.preventDefault();
				$.ajax({
					type:"POST",
					url: "includes/insert-anathesi.php",
					success: function(response){
						
						//alert(response);
						location.reload();
						
					},
					error: function(request, status, error) {
						alert(error);
						//alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
					}
				});
			});
		 });
		 
		$(function() {
			
			var normalize = function( term ) {
				var ret = "";
				for ( var i = 0; i < term.length; i++ ) {
					ret += accentMap[ term.charAt(i) ] || term.charAt(i);
				}
				return ret;
			};
			$( "#didaskon" ).autocomplete({ // Autocomplete στο συγκεκριμένο πεδίο 
				matchCase: false,
				source: function(req, response) { // Έλεγξε μόνο το πρώτο γράμμα της λέξης για matching 
				var re = $.ui.autocomplete.escapeRegex(req.term);												
				var matcher = new RegExp( "^" + re, "i" );															
				response($.grep(availableTags, function(item){return matcher.test(item.label); }) );	
				},
				minLength: 1, // Βγάλε αποτέλεσμα μετά τα 1 πρώτα γράμματα

				focus: function(event, ui) {
					var didaskonName = ui.item.label+" "+ui.item.name;
					event.preventDefault();	// prevent autocomplete from updating the textbox
					$(this).val(didaskonName); // manually update the textbox
				},
				select: function(event, ui) {
					var didaskonName = ui.item.label+" "+ui.item.name;
					var didaskonID = ui.item.value;
					event.preventDefault();	// prevent autocomplete from updating the textbox
					$(this).val(didaskonName); // manually update the textbox and hidden field
					$("#ID_Didaskonta").val(didaskonID); // Βάλε το ID μαθήματος στο πεδίο ID 
				}
			  
			}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			  return $( "<li>" )
				.append( "<a>" + item.label +" "+ item.name + "</a>" )
				.appendTo( ul );
			};
			
			$( "#mathima" ).autocomplete({ // Autocomplete στο συγκεκριμένο πεδίο 
				matchCase: false,
				source: function(req, response) { // Έλεγξε μόνο το πρώτο γράμμα της λέξης για matching 
				var re = $.ui.autocomplete.escapeRegex(req.term);												
				var matcher = new RegExp( "^" + re, "i" );															
				response($.grep(availableTags2, function(item){return matcher.test(item.label) || matcher.test(item.name); }) );	
				},
				minLength: 1, // Βγάλε αποτέλεσμα μετά τα 1 πρώτα γράμματα

				focus: function(event, ui) {
					var lessonTitle = ui.item.label;
					event.preventDefault();	// prevent autocomplete from updating the textbox
					$(this).val(lessonTitle); // manually update the textbox
				},
				select: function(event, ui) {
					var lessonTitle = ui.item.label;
					var lessonID = ui.item.value;
					event.preventDefault();	// prevent autocomplete from updating the textbox
				}
			  
			});
			
	    });
	</script>		

	<style>
  		.ui-tooltip {
   			 padding-bottom: 0px;
   			 padding-top: 0px;
   			 max-width: 600px;
   			 background: rgb(250,250,250);
   			 font-size: 11px;
   			 border: 10px;
   			 line-height: 22px;

  		}
  		.ui-button.ui-state-active .ui-button-text {
		    background-color: white;
		}
  	</style>

</head>

<body>
	<center>
		<table class="wrapper" width="1200px">
			<tbody>
				<tr>
					<td valign="top">
						<center>
							
							<?php
								require_once("includes/header3.php");
							?>
							<div id="anathesi" align="left" class="form_div">
								<form action="includes/insert-anathesi.php" method="post" id ="anathesi-form" name="anathesi-form">
									<table> 
										<tr>
										<p>
											<td><label for="mathima">Επιλέξτε Μάθημα:</label></td>
											<td style="padding-left:4px;"><input style="width:269px;" title="Πληκτρολογήστε το πρώτο γράμμα του μαθήματος" type="text" name="mathima" id="mathima"  /></td>
											<input type="hidden" name="ID_OMathima" id="ID_OMathima" readonly />
										</p>
										</tr>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="didaskon">Επιλέξτε Διδάσκοντα:</label></td>
													<td style="padding-left:4px;"><input style="width:269px;" title="Πληκτρολογήστε το πρώτο γράμμα του διδάσκοντα" type="text" name="didaskon" id="didaskon"  /></td>
													<input type="hidden" name="ID_Didaskonta" id="ID_Didaskonta" readonly />
												</p>
											</div>
										</tr>
										<tr>
											<td><label for="typos">Επιλέξτε τύπο:</label></td>
											<td style="padding-left:4px;">
												<span id="typos" class="buttonset">
												  <input type="radio" style="color: rgb(81, 183, 73);" id="theoria" name="typos" value="0" checked="checked"><label for="theoria">Θεωρία</label>
												  <input type="radio" id="frontistirio" name="typos" value="1"><label for="frontistirio">Φροντιστήριο</label>
												  <input type="radio" id="ergastirio" name="typos" value="2"><label for="ergastirio">Εργαστήριο</label>
												</div>
											</td>
										</tr>
										<tr  id="submit">
											<td><input  type="submit" value="Καταχώρηση" > </td>
										</tr>
									</table>
								</form>
							</div>

							<table id="table_id" class="display">
								<thead>
									<tr>
							            <th align="left">Μάθημα</th>
							            <th align="left">Τύπος</th>
							            <th align="left">Διδάσκων</th>
							            <th align="left">Τμήμα</th>
							            <th align="left"></th>
							        </tr>
								</thead>
								<tbody id="tbody_id">

									<!--
									//======================================================================//
									//																		//
									//			  Αναζήτηση στην βάση και γέμισμα του πίνακα				//
									//																		//
									//======================================================================//
									-->	
								
									<?php
									
										$sql_mathima = "select ID_Anathesis, ID_Parent, ID_TMathimatos, ID_Didaskonta, tmima from Anathesi"; // Αναζήτησε όλες τις καταχωρήσεις στον πίνακα των μαθημάτων
										$resutl_mathima = mysql_query($sql_mathima);
										$events = array();
										
										while($row_mathima = mysql_fetch_assoc($resutl_mathima)){ // Επαναλάβε μέχρι να τελειώσει ο πίνακας 
											
											
											$ID_Anathesis = $row_mathima['ID_Anathesis'];
											$ID_Parent = $row_mathima['ID_Parent'];
											$ID_TMathimatos = $row_mathima['ID_TMathimatos'];
											$ID_Didaskonta = $row_mathima['ID_Didaskonta'];
											$tmima = $row_mathima['tmima'];
											
											$sql_parent = "SELECT ID_OMathimatos FROM Anathesi_Parent WHERE ID_Anathesi_Parent = $ID_Parent";
											$result_parent = mysql_query($sql_parent);
											$row_parent = mysql_fetch_assoc($result_parent);
											
											$ID_OMathimatos = $row_parent['ID_OMathimatos'];

											$sql_mathima_orismos = "select onoma from Mathima_Orismos where ID_OMathimatos='$ID_OMathimatos'"; //Βρες το όνομα του μαθήματος για το συγκεκριμένο ID μαθήματος
											$result_mathima_orismos = mysql_query($sql_mathima_orismos);
											while($row_mathima_orismos = mysql_fetch_assoc($result_mathima_orismos)){
												$onoma_mathimatos = $row_mathima_orismos['onoma'];
											}

											$sql_didaskon = "select onoma, eponimo from Didaskon_Orismos where ID_Didaskonta='$ID_Didaskonta'";	//Βρες το όνομα του διδάσκοντα για το συγκεκριμένο ID διδάσκοντα
											$result_didaskon = mysql_query($sql_didaskon);
											while($row_didaskon = mysql_fetch_assoc($result_didaskon)){
												$onoma_didaskonta = $row_didaskon['onoma'];
												$eponimo_didaskonta = $row_didaskon['eponimo'];
											}

											$sql_typos = "select onoma from Mathima_Typos where ID_TMathimatos='$ID_TMathimatos'"; // Βρες το όνομα του τύπου για το συγκεκριμένο μάθημα 
											$result_typos = mysql_query($sql_typos);
											while($row_typos = mysql_fetch_assoc($result_typos)){
												$onoma_typou = $row_typos['onoma'];
											}
									?>
									<tr>
										<td><?php echo $onoma_mathimatos; ?></td> <!-- Εκτύπωσε τα αποτελέσματα -->
										<td><?php echo $onoma_typou ?></td>
										<td><?php echo $eponimo_didaskonta; echo " "; echo $onoma_didaskonta; ?></td>
										<td><?php echo $tmima ?></td>
										<td align="center"><div style="padding-top: 4px; padding-bottom: 4px; width: 17px;" onmouseover="$(this).find('img').attr('src','styles/basic/img/delete2.png');"  
										onmouseout="$(this).find('img').attr('src','styles/basic/img/delete.png');" onclick="onClick(<?php echo $ID_Anathesis ?>)"> <img src="styles/basic/img/delete.png" height="10" width="10" /></div></td>
									</tr>
									<?php
										
										} // Τέλος επανάληψης 
										
									?>
								
								</tbody>
							</table>
							
							<div id="delete_mathima_div" style= "display: none;"> 
								<form  id ="delete_mathima_form" name="delete_mathima-form">
									<input type="text" name="delete_anathesi_id" id="delete_anathesi_id"  />
								</form>
							</div>
							
						</center>
					<td>
				</tr>
			</tbody>
		</table>
	</center>
</body>

</html>	
<script>
	
	var availableTags = new Array(); // Φτιάξε έναν πίνακα availableTags 
	availableTags = <?php echo json_encode( $data ); ?>	// Κάνε echo τον πίνακα data 

	var availableTags2 = new Array(); // Φτιάξε έναν πίνακα availableTags 
	availableTags2 = <?php echo json_encode( $data2 ); ?> // Κάνε echo τον πίνακα data 
	
</script>