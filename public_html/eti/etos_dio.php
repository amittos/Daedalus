<script>
	$(document).ready(function() {
		$( "#add-repeat" ).buttonset();
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		var viewName = 'agendaWeek';


		
		var calendar = $('#calendar_dio').fullCalendar({

			header: {
				agendaWeek: 'true',
				left: 'prev,next today',
				center: 'title',
				right: 'agendaDay,agendaWeek,month'
			},
			minTime: 9,
			maxTime: 21,
			monthNames: ["Ιανουάριος","Φεβρουάριος","Μάρτιος","Απρίλιος","Μαιος","Ιούνιος","Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος" ], 
		    monthNamesShort: ['Ιαν','Φεβ','Μαρ','Απρ','Μαι','Ιουν','Ιουλ','Αυγ','Σεπ','Οκτ','Νοε','Δεκ'],
		    dayNames: [ 'Κυριακή', 'Δευτέρα', 'Τρίτη', 'Τετάρτη', 'Πέμπτη', 'Παρασκευή', 'Σάββατο' ],
		   	dayNamesShort: ['Κυρ', 'Δευ','Τρι','Τετ','Πεμ','Παρ','Σαβ'],
		   	buttonText: {
			    today: 'Σήμερα',
			    month: 'Μήνας',
			    week: 'Εβδομάδα',
			    day: 'Ημέρα'
		   	},
			weekNumbers: false,
			eventClick: function(event, element) {
				
				var start = event.start;
				var end = event.end;
				var startDate = $.fullCalendar.formatDate(start, "yyyy-MM-dd");
				var startTime = $.fullCalendar.formatDate(start, "HH:mm:ss");
				var endTime = $.fullCalendar.formatDate(end, "HH:mm:ss");
				
				$("#update-event-date").val(startDate);
				$("#update-start-time").val(startTime);
				$("#update-end-time").val(endTime);
				
				var weekday = new Array(7);
				weekday[0]=  "Κυριακή";
				weekday[1] = "Δευτέρα";
				weekday[2] = "Τρίτη";
				weekday[3] = "Τετάρτη";
				weekday[4] = "Πέμπτη";
				weekday[5] = "Παρασκευή";
				weekday[6] = "Σάββατο";
				
				$("#update-show-time").val(weekday[start.getDay()] + ' ' + $.fullCalendar.formatDate(start, "HH:mm") + '-' + $.fullCalendar.formatDate(end, "HH:mm"));
				
				$("#update-calendar-id").val(event.id);
				$("#update-title").val(event.title);
				
				/*
				if(event.repeats==1){
					$("#update-repeats").prop('checked', true);
					$("#update-repeat-options").show();
					if(event.repeat_freq == 7){
						$("#update-repeat-freq-7").prop('checked', true);
						$("#update-repeat-freq-14").prop('checked', false);
					}
					else if(event.repeat_freq == 14){
						$("#update-repeat-freq-7").prop('checked', false);
						$("#update-repeat-freq-14").prop('checked', true);
					}
				}
				else if(event.repeats==0){
					$("#update-repeats").prop("checked",false);
				}
				*/
				createUpdateDialog_dio(event) ;

			},

			firstDay: 1, // Πρώτη μέρα: Δευτέρα
			weekNumbers: true, // Εμφάνισε τον αριθμό της εβδομάδας
			defaultView: 'agendaWeek',	
			selectable: true, 
			selectHelper: true,
			allDaySlot: false,
			loading: function (bool) { 
		 	if (bool) 
			   	$("body").addClass("loading"); 
			},
			eventAfterAllRender: function(){
			   	$("body").removeClass("loading");
			},

			select: function(start, end, allDay) {
				var startDate = $.fullCalendar.formatDate(start, "yyyy-MM-dd");
				var startTime = $.fullCalendar.formatDate(start, "HH:mm:ss");
				if(!allDay){
					var endTime = $.fullCalendar.formatDate(end, "HH:mm:ss");
				}
				else{
					var endTime = '23:59:59';
				}
				$("#event-date-dio").val(startDate);
				$("#start-time-dio").val(startTime);
				$("#end-time-dio").val(endTime);
				
				createInsertDialog_dio();
			},

			//======================================================================//
			//																		//
			//				Διάβασμα στοιχείων για τον πίνακα Σοφούλης				//
			//																		//
			//======================================================================//

			eventResize: function(event,dayDelta,minuteDelta,revertFunc) {

				var start = event.start;
				var end = event.end;
				var startDate = $.fullCalendar.formatDate(start, "yyyy-MM-dd");
				var startTime = $.fullCalendar.formatDate(start, "HH:mm:ss");
				var endTime = $.fullCalendar.formatDate(end, "HH:mm:ss");
			  
			    $("#update-calendar-id").val(event.id);
				$("#update-event-date").val(startDate);
				$("#update-start-time").val(startTime);
				$("#update-end-time").val(endTime);
				$("#update-day-delta").val(dayDelta);
				
				$.ajax({
					type:"POST",
					url: "includes/update-series-event-datetime.php",
					data: $('#update-event-form').serialize(),
					success: function(){
						$('#calendar_dio').fullCalendar( 'refetchEvents' )
						},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την επεξεργασία. Παρακαλώ προσπαθήστε ξανά.");
					}
				});
				
				/*
				$( "#update-dialog-confirm" ).dialog({
					resizable: false,
					height:140,
					modal: true,
					buttons: {
						"This and future": function() {
							$.ajax({
								type:"POST",
								url: "includes/update-future-event-datetime.php",
								data: $('#update-event-form').serialize(),
								success: function(){
									$('#calendar').fullCalendar( 'refetchEvents' )
									},
								error: function(){
									alert("Υπήρξε ένα πρόβλημα κατά την διαγραφή. Παρακαλώ προσπαθήστε ξανά.");
								}
							});
							$( this ).dialog( "close" );
						},
						"All events": function() {
							$.ajax({
								type:"POST",
								url: "includes/update-series-event-datetime.php",
								data: $('#update-event-form').serialize(),
								success: function(){
									$('#calendar').fullCalendar( 'refetchEvents' )
									},
								error: function(){
									alert("Υπήρξε ένα πρόβλημα κατά την διαγραφή. Παρακαλώ προσπαθήστε ξανά.");
								}
							});
							$( this ).dialog( "close" );
						},
					}
				}); */
				
			},
			eventDrop: function(event,dayDelta,minuteDelta,allDay,revertFunc) {

				var start = event.start;
				var end = event.end;
				var startDate = $.fullCalendar.formatDate(start, "yyyy-MM-dd");
				var startTime = $.fullCalendar.formatDate(start, "HH:mm:ss");
				var endTime = $.fullCalendar.formatDate(end, "HH:mm:ss");
			  
			    $("#update-calendar-id").val(event.id);
				$("#update-event-date").val(startDate);
				$("#update-start-time").val(startTime);
				$("#update-end-time").val(endTime);
				$("#update-day-delta").val(dayDelta);
				
				$.ajax({
					type:"POST",
					url: "includes/update-series-event-datetime.php",
					data: $('#update-event-form').serialize(),
					success: function(){
						$('#calendar_dio').fullCalendar( 'refetchEvents' )
						},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την επεξεργασία. Παρακαλώ προσπαθήστε ξανά.");
					}
				});
				
				/*
				$( "#update-dialog-confirm" ).dialog({
					resizable: false,
					height:140,
					modal: true,
					buttons: {
						"This and future": function() {
							$.ajax({
								type:"POST",
								url: "includes/update-future-event-datetime.php",
								data: $('#update-event-form').serialize(),
								success: function(){
									$('#calendar').fullCalendar( 'refetchEvents' )
									},
								error: function(){
									alert("Υπήρξε ένα πρόβλημα κατά την επεξεργασία. Παρακαλώ προσπαθήστε ξανά.");
								}
							});
							$( this ).dialog( "close" );
						},
						"All events": function() {
							$.ajax({
								type:"POST",
								url: "includes/update-series-event-datetime.php",
								data: $('#update-event-form').serialize(),
								success: function(){
									$('#calendar').fullCalendar( 'refetchEvents' )
									},
								error: function(){
									alert("Υπήρξε ένα πρόβλημα κατά την επεξεργασία. Παρακαλώ προσπαθήστε ξανά.");
								}
							});
							$( this ).dialog( "close" );
						},
					}
				}); */
				
				/*$.ajax({
						type:"POST",
						url: "includes/update-future-event-datetime.php",
						data: $('#update-event-form').serialize(),
						success: function(){
							$('#calendar').fullCalendar( 'refetchEvents' );
						},
						error: function(){
							alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
							revertFunc();
						}
				});*/
			

			},
			editable: true,
			events: '/project/daedalus/calendar_feeds/etos_dio_feed.php' // Τράβα τα events από το ανάλογο feed 															
		});
		
	});
	
	function createInsertDialog_dio() {

		$('#add-event-dio')
		.dialog({
			height: 'auto',
			width: 'auto',
			autoResize:true,
			modal: true,
			resizable: false,
			
			open: function(){
				$("#title").attr("#tabindex","1");
				$("#repeat-options").hide();
			},
			buttons: {
				"Αποθήκευση": function() { // Υλοποίηση του button save
					$.ajax({
						type:"POST",
						url: "includes/add-event.php",
						data: $('#add-event-form-dio').serialize(),
						success: function(){
							$('#calendar_dio').fullCalendar( 'refetchEvents' );
							
							//Αρχικοποίηση όλων των πεδίων
							$('#title-dio').val("");
							$('#aithousa-dio').val("");
							$('#ID-Anathesis-dio').val("");
							$('#event-date-dio').val("");
							$('#start-time-dio').val("");
							$('#end-time-dio').val("");
							$('#repeats-dio').prop('checked', false);
							$('#repeat-freq-7').prop('checked', true);
							$('#repeat-freq-14').prop('checked', false);
						},
						error: function(){
							alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						}
					});
					$(this).dialog("close");
				},

				"Ακύρωση": function() { // Υλοποίηση του button cancel
					$(this).dialog("close");
				}
			},
		});
		
		//$('#add-event').dialog("open");
	
	}

	//======================================================================//
	//																		//
	//							Create Event								//
	//																		//
	//======================================================================//
	
	function createUpdateDialog_dio(event) {

		$('#update-event')
		.dialog({
			height: 'auto',
			width: '421px',
			autoResize:true,
			modal: true,
			resizable: false,
			
			open: function(){
				$("#update-title").attr("#tabindex","1");
				$("#repeat-options").hide();
			},
			buttons: {
				"Διαγραφή": function(){
				
				
					$( "#dialog-confirm" ).dialog({
						resizable: false,
						height:140,
						modal: true,
						buttons: {
							"Ναι": function() {
								$.ajax({
									type:"POST",
									url: "includes/delete-series-events.php",
									data: $('#update-event-form').serialize(),
									success: function(){
										$('#calendar_dio').fullCalendar( 'refetchEvents' );
									},
									error: function(){
										alert("Υπήρξε ένα πρόβλημα κατά την διαγραφή. Παρακαλώ προσπαθήστε ξανά.");
									}
								});
								$( this ).dialog( "close" );
							},
							"Ακύρωση": function() { // Υλοποίηση του button cancel
								$(this).dialog("close");
							},
						}
					});
							
					$( this ).dialog( "close" );
				},
				"Ακύρωση": function() { // Υλοποίηση του button cancel
					$(this).dialog("close");
				}
			},
		});
		
		//$('#add-event').dialog("open");
	
	}		
	
	$( "#create-user" )
	  .button()
	  .click(function() {
		$( "#add-event" ).dialog( "open" );
	});

	//======================================================================//
	//																		//
	//								Autocomplete							//
	//																		//
	//======================================================================//

	$(function() {
		
		 var accentMap = {
		  "ά": "α",
		  "έ": "ε",
		  "ύ": "υ",
		  "ί": "ι",
		  "ό": "ο",
		  "ή": "η",
		  "ώ": "ω",
		};
		$( "#title-dio" ).autocomplete({ // Autocomplete στο συγκεκριμένο πεδίο 
			matchCase: false,
			source: function(req, response) { // Έλεγξε μόνο το πρώτο γράμμα της λέξης για matching 
			var re = $.ui.autocomplete.escapeRegex(req.term);												
			var matcher = new RegExp( "^" + re, "i" );															
			response($.grep(mathimataTags, function(item){return matcher.test(item.label); }) );	
			},
			minLength: 1, // Βγάλε αποτέλεσμα μετά τα 3 πρώτα γράμματα

			focus: function(event, ui) {
				 // prevent autocomplete from updating the textbox
				//$(this).val(lessonTitle); // manually update the textbox
			},
			select: function(event, ui) {
				event.preventDefault(); // prevent autocomplete from updating the textbox
				var lessonTitle = ui.item.label;
				var anathesiID = ui.item.value;
				$(this).val(lessonTitle); // manually update the textbox and hidden field
				$("#ID-Anathesis-dio").val(anathesiID); // Βάλε το ID μαθήματος στο πεδίο ID 
				
				//alert(anathesiID);
				
				var radioHtml = '<td><label for="typosID">Τύπος:</label></td><td>';
				if(ui.item.theoria==1){
					//alert("Theoriaaa");
					radioHtml+= '<input type="radio" value= "theoria" name="typosRadio" id="typosRadio-theoria" align="bottom" checked="checked"><label for="typosRadio-theoria">Θεωρία</label>';
				}
				if(ui.item.ergastirio==1){
					//alert("Εργαστήριο");
					radioHtml+= '<input type="radio" value= "ergastirio" name="typosRadio" id="typosRadio-ergastirio" align="bottom"><label for="typosRadio-ergastirio">Εργαστήριο</label>';
				}
				if(ui.item.frontistirio==1){
					//alert("Φροντιστήριο");
					radioHtml+= '<input type="radio" value= "frontistirio" name="typosRadio" id="typosRadio-frontistirio" align="bottom"><label for="typosRadio-frontistirio">Φροντιστήριο</label>';
				}
				radioHtml += '</td>';
				
				//alert(radioHtml);
				
				var typosDiv = document.getElementById("typosRadio-dio");
				
				$('#add-event-table-dio tr:last').after('<tr>...</tr><tr>...</tr>');
				
			}
		  
		});
		
		/*$( "#update-title" ).autocomplete({ // Autocomplete στο συγκεκριμένο πεδίο 
			matchCase: false,
			source: function(req, response) { // Έλεγξε μόνο το πρώτο γράμμα της λέξης για matching 
			var re = $.ui.autocomplete.escapeRegex(req.term);												
			var matcher = new RegExp( "^" + re, "i" );															
			response($.grep(mathimataTags , function(item){return matcher.test(item.label); }) );	
			},
			minLength: 1, // Βγάλε αποτέλεσμα μετά τα 3 πρώτα γράμματα

			focus: function(event, ui) {
				var lessonTitle = ui.item.label;
				event.preventDefault();	// prevent autocomplete from updating the textbox
				$(this).val(lessonTitle); // manually update the textbox
			},
			select: function(event, ui) {
				var lessonTitle = ui.item.label;
				var lessonID = ui.item.value;
				event.preventDefault();	// prevent autocomplete from updating the textbox
				$(this).val(lessonTitle); // manually update the textbox and hidden field
				$("#ID-OMathima").val(lessonID); // Βάλε το ID μαθήματος στο πεδίο ID 
			}
		  
		});*/

		$( "#aithousa-dio" ).autocomplete({ // Autocomplete στο συγκεκριμένο πεδίο 
			matchCase: false,
			source: function(req, response) { // Έλεγξε μόνο το πρώτο γράμμα της λέξης για matching 
			var re = $.ui.autocomplete.escapeRegex(req.term);												
			var matcher = new RegExp( "^" + re, "i" );															
			response($.grep(availableTags_aithousa, function(item){return matcher.test(item.label); }) );	
			},
			minLength: 1, // Βγάλε αποτέλεσμα μετά τα 1 πρώτα γράμματα

			focus: function(event, ui) {
				var aithousaTitle = ui.item.label;
				event.preventDefault(); // prevent autocomplete from updating the textbox
				//$(this).val(aithousaTitle); // manually update the textbox
			},
			select: function(event, ui) {
				var aithousaTitle = ui.item.label;
				var aithousaID = ui.item.value;
				event.preventDefault(); // prevent autocomplete from updating the textbox
				$(this).val(aithousaTitle); // manually update the textbox and hidden field
				$("#ID-Aithousas-dio").val(aithousaID); // Βάλε το ID μαθήματος στο πεδίο ID 
			}
		  
		});
		
    });
		
</script>