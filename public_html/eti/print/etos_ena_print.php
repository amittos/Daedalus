<script>
	$(document).ready(function() {
		$( "#add-repeat" ).buttonset();
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		var viewName = 'agendaWeek';


		
		var calendar = $('#calendar_ena').fullCalendar({

			header: {
				agendaWeek: 'true',
				left: '',
				center: 'Έτος 1ο',
				right: ''
			},
			minTime: 9,
			maxTime: 21,
			aspectRatio: 1,
			weekends: false,
			allDaySlot: false,
			slotEventOverlap: false,
			firstDay: 1, // Πρώτη μέρα: Δευτέρα
			weekNumbers: false,
			defaultView: 'agendaWeek',	
			selectable: false, 
			selectHelper: false,
			monthNames: ["Ιανουάριος","Φεβρουάριος","Μάρτιος","Απρίλιος","Μαιος","Ιούνιος","Ιούλιος", "Αύγουστος", "Σεπτέμβριος", "Οκτώβριος", "Νοέμβριος", "Δεκέμβριος" ], 
		    monthNamesShort: ['Ιαν','Φεβ','Μαρ','Απρ','Μαι','Ιουν','Ιουλ','Αυγ','Σεπ','Οκτ','Νοε','Δεκ'],
		    dayNames: [ 'Κυριακή', 'Δευτέρα', 'Τρίτη', 'Τετάρτη', 'Πέμπτη', 'Παρασκευή', 'Σάββατο' ],
		   	dayNamesShort: ['Κυρ', 'Δευ','Τρι','Τετ','Πεμ','Παρ','Σαβ'],
		   	buttonText: {
			    today: 'Σήμερα',
			    month: 'Μήνας',
			    week: 'Εβδομάδα',
			    day: 'Ημέρα'
		   	},
			loading: function (bool) { 
		 	if (bool) 
			   	$("body").addClass("loading"); 
			},
			eventAfterAllRender: function(){
				isReady_ena = 1;
			    printPage();
			},
			columnFormat: {
                month: 'dddd',    
                week: 'dddd', 
                day: 'dddd'  
            },
			
			
			
			editable: false,
			events: '/project/daedalus/calendar_feeds/etos_ena_feed.php', // Τράβα τα events από το json-events.php 															
		});
		
	});
	

	
</script>