<?php
	session_start();
	/*
	if($_SESSION['user_role'] != "2" && $_SESSION['user_role'] != "4"){
		header( 'Location: not_authorized.php' ) ;
	}
	*/
?>
<?php
	include ("database.php");
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" lang="el" xml:lang="en"> 
<head>
	
	<!-- JQuery Libray -->
	<script class="jsbin" src="http://code.jquery.com/jquery-1.11.0.js"></script>
	<script src="https://github.com/arshaw/fullcalendar/blob/master/src/_loader.js"></script>
	<script src='jquery-ui.custom.min.js'></script>
	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<link rel="stylesheet" href="styles/basic/style.css">

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="jquery.dataTables.css">  <!-- Local Installation -->
	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>


	<title> Daedalus </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Language" content="el">	
	<link rel="stylesheet" href="styles/basic/input.css" type="text/css" media="screen"/>
	<link rel="shortcut icon" href="styles/basic/img/favicon.ico" />

	<script>

		$(document).ready( function () { //Script του DataTable
		    $('#table_id').DataTable();
		} );

		$(function() {
	   		$( document ).tooltip();
	    });

		//======================================================================//
		//																		//
		//							Διαγραφή Διδάσκοντα							//
		//																		//
		//======================================================================//
		
		function onDeleteClick(ID_Lesson){ 	//OnClick του delete button
		
			$("#delete_lesson_id" ).val(ID_Lesson);
			
			$.ajax({
					type:"POST",
					url: "includes/delete-lesson.php",
					data: $('#delete_lesson_form').serialize(),
					success: function(){
						
						location.reload();
						
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την διαγραφή. Παρακαλώ προσπαθήστε ξανά.");
					}
				});
		
		}

		//======================================================================//
		//																		//
		//						Επεξεργασία Διδάσκοντα 							//
		//																		//
		//======================================================================//
		 
		 function createEditDialog(ID_Lesson,onoma,eponimo,mail,url,phone_work,phone_mobile) {

			$('#edit-tutor-div')
			.dialog({
				height: 'auto',
				width: 'auto',
				autoResize:true,
				modal: true,
				resizable: false,
				
				open: function(){

					$("#edit-id-didaskonta").val(ID_Didaskonta);
					$("#edit-onoma").val(onoma);
					$("#edit-eponimo").val(eponimo);
					$("#edit-url").val(url);
					$("#edit-mail").val(mail);
					$("#edit-phone-work").val(phone_work);
					$("#edit-phone-mobile").val(phone_mobile);
					
				},
				buttons: {
					"Αποθήκευση": function() {	// Υλοποίηση του button save
						$.ajax({
							type:"POST",
							url: "includes/update-tutor.php",
							data: $('#edit-tutor-form').serialize(),
							success: function(){
								
								//Αρχικοποίηση όλων των πεδίων
								$("#edit-id-didaskonta").val("");
								$("#edit-onoma").val("");
								$("#edit-eponimo").val("");
								$("#edit-mail").val("");
								$("#edit-url").val("");
								$("#edit-phone-work").val("");
								$("#edit-phone-mobile").val("");
								
								location.reload(); // =refresh page
								
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
							}
						});
						$(this).dialog("close");
					},

					"Ακυρο": function() { // Υλοποίηση του button cancel
						$(this).dialog("close");
					}
				},
			});
		
		}

		//======================================================================//
		//																		//
		//						Εισαγωγή Διδάσκοντα 							//
		//																		//
		//======================================================================//
		
		function createInsertDialog() {

			$('#insert-tutor-div')
			.dialog({
				height: 'auto',
				width: 'auto',
				autoResize:true,
				modal: true,
				resizable: false,
				
				open: function(){
						
				},
				buttons: {
					"Αποθήκευση": function() {	// Υλοποίηση του button save
						$.ajax({
							type:"POST",
							url: "includes/insert-tutor.php",
							data: $('#insert-tutor-form').serialize(),
							success: function(){
								
								location.reload(); // =refresh page
								
							},
							error: function(){
								
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								//Αρχικοποίηση όλων των πεδίων
								$("#edit-id-didaskonta").val("");
								$("#onoma").val("");
								$("#eponimo").val("");
								$("#mail").val("");
								$("#url").val("");
								$("#phone_work").val("");
								$("#phone_mobile").val("");
								
							}
						});
						$(this).dialog("close");
					},

					"Ακυρο": function() { // Υλοποίηση του button cancel
						$(this).dialog("close");
						//Αρχικοποίηση όλων των πεδίων
								$("#edit-id-didaskonta").val("");
								$("#onoma").val("");
								$("#eponimo").val("");
								$("#mail").val("");
								$("#url").val("");
								$("#phone_work").val("");
								$("#phone_mobile").val("");
					}
				},
			});
		
		}

		//======================================================================//
		//																		//
		//					Link για τη Διαθεσιμότητα Διδάσκοντα 				//
		//																		//
		//======================================================================//
		 
		function editDiathesimotita(ID_Didaskonta){
		
			var toRedirect = "tutor_edit_diathesimotita.php?id="+ID_Didaskonta;
			window.location.href = toRedirect;
		
		}

		//======================================================================//
		//																		//
		//					Autocomplete με ονόματα διδασκόντων 				//
		//																		//
		//======================================================================//

		$(function() {
			
			 var accentMap = {
			  "ά": "α",
			  "έ": "ε",
			  "ύ": "υ",
			  "ί": "ι",
			  "ό": "ο",
			  "ή": "η",
			  "ώ": "ω",
			};
			
			$( "#didaskon" ).autocomplete({ // Autocomplete στο συγκεκριμένο πεδίο 
				matchCase: false,
				source: function(req, response) { // Έλεγξε μόνο το πρώτο γράμμα της λέξης για matching 
				var re = $.ui.autocomplete.escapeRegex(req.term);												
				var matcher = new RegExp( "^" + re, "i" );															
				response($.grep(availableTags, function(item){return matcher.test(item.label); }) );	
				},
				minLength: 2, // Βγάλε αποτέλεσμα μετά τα 2 πρώτα γράμματα

				focus: function(event, ui) {
					var didaskonName = ui.item.label+" "+ui.item.name;
					event.preventDefault();	// prevent autocomplete from updating the textbox
					$(this).val(didaskonName); // manually update the textbox
				},
				select: function(event, ui) {
					var didaskonName = ui.item.label+" "+ui.item.name;
					var didaskonID = ui.item.value;
					event.preventDefault();	// prevent autocomplete from updating the textbox
					$(this).val(didaskonName);	// manually update the textbox and hidden field
					$("#ID-Didaskonta").val(didaskonID); // Βάλε το ID μαθήματος στο πεδίο ID 
				}
			  
			}).data( "ui-autocomplete" )._renderItem = function( ul, item ) {
			  return $( "<li>" )
				.append( "<a>" + item.label +" "+ item.name + "</a>" )
				.appendTo( ul );
			};
			
	    });

	</script>
	
	<style>
  		.ui-tooltip {
   			 padding-bottom: 0px;
   			 padding-top: 0px;
  		}
  	</style>

</head>

<body link="#5C5C5C" vlink="#7A7A7A" alink="#A3A3A3"> <!-- Άλλαξε το χρώμα των links -->
	<center>
		<table class="wrapper">
			<tbody>
				<tr>
					<td valign="top">
						<center>

							<?php
								require_once("includes/header3.php");
							?>
							<div id="insert-tutor-div" title="Εισαγωγή Καθηγητή" style= "display: none;">
								<form action="includes/insert-tutor.php" method="post" id ="insert-tutor-form" name="insert-tutor-form">
									<table>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="onoma">Όνομα:</label></td>
													<td><input type="text" name="onoma" id="onoma"  /></td>
												</p>
											</div>
										</tr>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="eponimo">Επώνυμο:</label></td>
													<td><input type="text" name="eponimo" id="eponimo"  /></td>
												</p>
											</div>
										</tr>
										<tr>
										<p>
											<td><label for="mail">E-Mail:</label></td>
											<td><input type="text" name="mail" id="mail" value="" /></td>
										</p>
										</tr>
										<tr>
										<p>
											<td><label for="url">Ιστοσελίδα:</label></td>
											<td><input type="text" name="url" id="url"  value=""/></td>
										</p>
										</tr>
										<tr>
										<p>
											<td><label for="phone_work">Τηλέφωνο Γραφείου:</label></td>
											<td><input type="text" name="phone_work" id="phone_work" value="" /></td>
										</p>
										</tr>
										<tr>
										<p>
											<td><label for="phone_mobile">Τηλέφωνο Κινήτου:</label></td>
											<td><input type="text" name="phone_mobile" id="phone_mobile"  value=""/></td>
										</p>
										</tr>
										<!--<tr  id="submit">
											<td><input type="submit" value="Εισαγωγή"> </td>
										</tr> -->
									</table>
								</form>
							</div>
							
							<img  align="right" height="20px" width="100px" style="margin-bottom:30px; margin-top:30px; position: relative; right:-604px;" src="styles/basic/img/add.png" onclick="createInsertDialog();" />
							
							<table id="table_id" class="display">
								<thead>
									<tr>
							            <th align="left">Διδάσκων</th>
							            <th align="left">E-Mail</th>
							            <th align="left">Ιστοσελίδα</th>
							            <th align="left">Τηλέφωνο</th>
							            <th align="left">Κινητό</th>
							            <th></th>
							            <th></th>
							            <th></th>
							        </tr>
								</thead>
								<tbody>	
								<?php
									
									$sql_1 = "select ID_Didaskonta, onoma, eponimo, mail, url, am, at, phone_mobile, phone_work, priority from Didaskon_Orismos";
											
									$result_1 = mysql_query($sql_1);
									
									while($row_1 = mysql_fetch_assoc($result_1)){
										
										$id_didaskonta = $row_1['ID_Didaskonta'];
										$onoma = $row_1['onoma'];
										$eponimo = $row_1 ['eponimo'];
										$mail = $row_1['mail'];
										$url = $row_1['url'];
										$am = $row_1['am'];
										$at = $row_1['at'];
										$phone_mobile = $row_1['phone_mobile'];
										$phone_work = $row_1['phone_work'];
										$priority = $row_1['priority'];

								?>
								
									<tr>
										<td ><?php echo $eponimo; echo " "; echo $onoma; ?></td>
										<td style="word-wrap:break-word;width:150px"><a href=mailto:<?php echo $mail; ?> style="text-decoration:none;"><?php echo $mail; ?></td>
										<?php 
											if($url == ""){
										?>
												<td style="word-wrap:break-word;width:160px;"></td>
										<?php	
											}
											else{
										?>
												<td style="word-wrap:break-word;width:160px;"><a href=<?php echo $url; ?> target="_blank" style="text-decoration:none;">Webpage</a></td>
										<?php	
											}
										?>
										<td style="word-wrap:break-word;width:150px"><?php echo $phone_work; ?></td>
										<td style="word-wrap:break-word;width:150px"><?php echo $phone_mobile; ?></td>
										<td align="center"><div title="Διαθεσιμότητα διδάσκοντα" style="padding-top: 4px; padding-bottom: 4px; width: 17px;" onmouseover="$(this).find('img').attr('src','styles/basic/img/agenda2.png');"  
										onmouseout="$(this).find('img').attr('src','styles/basic/img/agenda.png');" onclick="editDiathesimotita(<?php echo $id_didaskonta ?>)"> <img src="styles/basic/img/agenda.png"style="height:20px; width:20px;" /></div></td>
										<td align="center"><div title="Επεξεργασία στοιχείων διδάσκοντα" style="padding-top: 4px; padding-bottom: 4px; width: 17px;" onmouseover="$(this).find('img').attr('src','styles/basic/img/edit2.png');"  
										onmouseout="$(this).find('img').attr('src','styles/basic/img/edit.png');" onclick="createEditDialog(<?php echo "'".$id_didaskonta ."','".  $onoma ."','". $eponimo ."','". $mail ."','". $url ."','". $phone_work ."','". $phone_mobile."'" ?>);"> <img src="styles/basic/img/edit.png" style="height:20px; width:20px;" /></div></td>
										<td align="center"><div title="Διαγραφή διδάσκοντα" style="padding-top: 4px; padding-bottom: 4px; width: 17px;" onmouseover="$(this).find('img').attr('src','styles/basic/img/delete2.png');"  
										onmouseout="$(this).find('img').attr('src','styles/basic/img/delete.png');" onclick="onDeleteClick(<?php echo $id_didaskonta ?>)"> <img src="styles/basic/img/delete.png" style="height:20px; width:20px;" /></div></td>
									</tr>
										
								<?php
									
									} //end while
									
								?>
								</tbody>					
							</table>
							
							<div id="delete_tutor_div" style= "display: none;"> 
								<form  id ="delete_tutor_form" name="delete_tutor-form">
									<input type="text" name="delete_tutor_id" id="delete_tutor_id"  />
								</form>
							</div>
							
							
							<div id="edit-tutor-div" style="display:none;">
								<form action="includes/update-tutor.php" method="post"	 id ="edit-tutor-form" name="edit-tutor-form">
									<table>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="edit-onoma">Όνομα:</label></td>
													<td><input type="text" name="edit-onoma" id="edit-onoma"  /></td>
													<td><input type="hidden" name="edit-id-didaskonta" id="edit-id-didaskonta"  /></td>
												</p>
											</div>
										</tr>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="edit-eponimo">Επώνυμο:</label></td>
													<td><input type="text" name="edit-eponimo" id="edit-eponimo"  /></td>
												</p>
											</div>
										</tr>
										<tr>
										<p>
											<td><label for="edit-mail">E-Mail:</label></td>
											<td><input type="text" name="edit-mail" id="edit-mail" value="" /></td>
										</p>
										</tr>
										<tr>
										<p>
											<td><label for="edit-url">Ιστοσελίδα:</label></td>
											<td><input type="text" name="edit-url" id="edit-url"  value=""/></td>
										</p>
										</tr>
										<tr>
										<p>
											<td><label for="edit-phone-work">Τηλέφωνο Γραφείου:</label></td>
											<td><input type="text" name="edit-phone-work" id="edit-phone-work" value="" /></td>
										</p>
										</tr>
										<tr>
										<p>
											<td><label for="edit-phone-mobile">Τηλέφωνο Κινήτου:</label></td>
											<td><input type="text" name="edit-phone-mobile" id="edit-phone-mobile"  value=""/></td>
										</p>
										</tr>
										
									</table>
								</form>
							</div>
							
						</center>
					<td>
				</tr>
			</tbody>
		</table>
	</center>
</body>
</html>	

<script>
	var availableTags = new Array(); // Φτιάξε έναν πίνακα availableTags 
	availableTags = <?php echo json_encode( $data ); ?>	// Κάνε echo τον πίνακα data 	
</script>