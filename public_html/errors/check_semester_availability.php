
<?php
	
	function check_semester_availability($ID_Event_Parent, $Xeimerino_Eksamino, $Earino_Eksamino) {

		$array_of_invalid_semester_hours = array();

		$sql_initial_availability_classroom = "SELECT weekday, start_time, end_time, ID_Aithousas, ID_Anathesis, title FROM Event_Parent WHERE parent_id = '$ID_Event_Parent'";
		$result_initial_availability_classroom = mysql_query($sql_initial_availability_classroom);
		while( $row_initial_availability_classroom = mysql_fetch_assoc($result_initial_availability_classroom) ){

			// Εκχώρησε τις τιμές 
			$weekday = $row_initial_availability_classroom['weekday'];
			$start_time = $row_initial_availability_classroom['start_time'];
			$end_time = $row_initial_availability_classroom['end_time'];
			$ID_Aithousas = $row_initial_availability_classroom['ID_Aithousas'];
			$ID_Anathesis = $row_initial_availability_classroom['ID_Anathesis'];

			$lesson_title = $row_initial_availability_classroom['title'];

			// Μετάτρεψε τις ώρες σε διψήφιους αριθμούς 
			$clean_start_time = time_converter($start_time);
			$clean_end_time = time_converter($end_time);
			$duration = duration($clean_start_time, $clean_end_time);

			
			$sql_other_events = "SELECT parent_id, weekday, start_time, end_time, ID_Anathesis, title FROM Event_Parent WHERE parent_id != '$ID_Event_Parent'";

			$result_other_events = mysql_query($sql_other_events);
			while( $row_other_events = mysql_fetch_assoc($result_other_events) ){
				
				// Εκχώρησε τις τιμές 
				$id_current = $row_other_events['parent_id'];
				$weekday_current = $row_other_events['weekday'];
				$start_time_current = $row_other_events['start_time'];
				$end_time_current = $row_other_events['end_time'];
				$ID_Anathesis_current = $row_other_events['ID_Anathesis'];
				$lesson_title_current = $row_other_events['title'];

				// Μετάτρεψε τις ώρες σε διψήφιους αριθμούς 
				$clean_start_time_current = time_converter($start_time_current);
				$clean_end_time_current = time_converter($end_time_current);
				$duration_current = duration($clean_start_time_current, $clean_end_time_current);


				$sql_anathesi = "SELECT ID_Parent FROM Anathesi WHERE ID_Anathesis = '$ID_Anathesis_current' ";
				$result_anathesi = mysql_query($sql_anathesi);
				$row_anathesi = mysql_fetch_assoc($result_anathesi);
				$ID_Anathesi_Parent = $row_anathesi['ID_Parent'];

				$sql_anathesi_parent = "SELECT ID_OMathimatos FROM Anathesi_Parent WHERE ID_Anathesi_Parent = '$ID_Anathesi_Parent' ";
				$result_anathesi_parent = mysql_query($sql_anathesi_parent);
				$row_anathesi_parent = mysql_fetch_assoc($result_anathesi_parent);
				$ID_OMathimatos = $row_anathesi_parent['ID_OMathimatos'];

				$sql_mathima = "SELECT ID_Eksaminou FROM Mathima_Orismos WHERE ID_OMathimatos = '$ID_OMathimatos' ";
				$result_mathima = mysql_query($sql_mathima);
				$row_mathima = mysql_fetch_assoc($result_mathima);
				$ID_Eksaminou = $row_mathima['ID_Eksaminou'];
				
				if($ID_Eksaminou >= $Xeimerino_Eksamino &&  $ID_Eksaminou <= $Earino_Eksamino){
					
					
					$error_found = 0;

					//Ελεγξε κάθε ID_Hour_Per_Day του αρχικού μαθήματος με κάθε ID_Hour_Per_Day των άλλων μαθημάτων
					for ($i=0; $i<$duration; $i++) {

						//Αν έχουμε βρει έστω και ένα error βγαίνουμε απο την επανάληψη
						if($error_found == 1) break;

						//ID_Hour_Per_Day αρχικού μαθήματος
						$ID_Hour_Per_Day = find_ID_Hour_Per_Day($weekday, $clean_start_time+$i);
						
						
						for ($j=0; $j<$duration_current; $j++) {
							
							//ID_Hour_Per_Day του κάθε άλλου μαθήματος
							$ID_Hour_Per_Day_current = find_ID_Hour_Per_Day($weekday_current, $clean_start_time_current+$j);
							
							if($ID_Hour_Per_Day == $ID_Hour_Per_Day_current){
								$to_be_inserted['lesson_title'] = $lesson_title;
								$to_be_inserted['conflict_lesson_title'] = $lesson_title_current;
								
								$array_of_invalid_semester_hours[] = $to_be_inserted;

								//Αφου βρήκαμε error αλλάζουμε την μεταβλητή error_found σε 1 και κάνουμε break
								$error_found = 1;
								break;
							}
						
						}
					
					}
				
				}

			}
			
		}
		return $array_of_invalid_semester_hours;
	}

?>