<?php
	$array_of_invalid_classroom_types = array();
	function check_classroom_type($ID_Event_Parent) {

		// Βρες τον τύπο μαθήματος και το ID της Αίθουσας για το συγκεκριμένο event 
		$sql = "SELECT title, ID_Anathesis, ID_Aithousas FROM Event_Parent WHERE parent_id = '$ID_Event_Parent'";
		$result = mysql_query($sql);
		$row = mysql_fetch_assoc($result);

		$lesson_title = $row['title'];
		$ID_Anathesis = $row['ID_Anathesis'];
		$ID_Aithousas = $row['ID_Aithousas'];

		// Τώρα που έχεις το ID της ανάθεσης, δες τι τύπο έχει η συγκεκριμένη αναθεση
		$sql_anathesi = "SELECT ID_TMathimatos FROM Anathesi WHERE ID_Anathesis = '$ID_Anathesis'";
		$result_anathesi = mysql_query($sql_anathesi);

		$typos_mathimatos = $row_anathesi['ID_TMathimatos'];

		// Πάρε το ID της αίθουσας, δες τι τύπο έχει η συγκεκριμένη αίθουσα 
		// και σύγκρινέ την καταχώρηση σου με τον τύπο του μαθήματος, 
		// για να δεις ποιος είναι ο συνδυασμός.
		//
		// Κάθε συνδυασμός επιστρέφει κάτι διαφορετικό 
		$sql_aithousa = "SELECT onoma, ID_TAithousas FROM Aithousa_Orismos WHERE ID_Aithousas = '$ID_Aithousas'";
		$result_aithousa = mysql_query($sql_aithousa);
		$row_aithousa = mysql_fetch_assoc($result_aithousa);

		$aithousa_name = $row_aithousa['onoma'];
		$typos_aithousas = $row_aithousa['ID_TAithousas'];

		// Μάθημα: Θεωρία, Αίθουσα: Θεωρία 
		if ( $typos_mathimatos == 0 AND $typos_aithousas == 1 ) {
			//do nothing
		}

		// Μάθημα: Θεωρία, Αίθουσα: Εργαστήριο 
		if ( $typos_mathimatos == 0 AND $typos_aithousas == 2 ) {
			$to_be_inserted['lesson_title'] = $lesson_title;
			$to_be_inserted['aithousa_name'] = $aithousa_name;
			$to_be_inserted['error_message'] = "Μάθημα: Θεωρία, Αίθουσα: Εργαστήριο";
			$array_of_invalid_classroom_types[] = $to_be_inserted;
		}

		// Μάθημα: Φροντιστήριο, Αίθουσα: Θεωρία 
		if ( $typos_mathimatos == 1 AND $typos_aithousas == 1 ) {
			//do nothing
		}

		// Μάθημα: Φροντιστήριο, Αίθουσα: Εργαστήριο 
		if ( $typos_mathimatos == 1 AND $typos_aithousas == 2 ) {
			$to_be_inserted['lesson_title'] = $lesson_title;
			$to_be_inserted['aithousa_name'] = $aithousa_name;
			$to_be_inserted['error_message'] = "Μάθημα: Φροντιστήριο, Αίθουσα: Εργαστήριο";
			$array_of_invalid_classroom_types[] = $to_be_inserted;
		}

		// Μάθημα: Εργαστήριο, Αίθουσα: Θεωρία 
		if ( $typos_mathimatos == 2 AND $typos_aithousas == 1 ) {
			$to_be_inserted['lesson_title'] = $lesson_title;
			$to_be_inserted['aithousa_name'] = $aithousa_name;
			$to_be_inserted['error_message'] = "Μάθημα: Εργαστήριο, Αίθουσα: Θεωρία";
			$array_of_invalid_classroom_types[] = $to_be_inserted;
		}

		// Μάθημα: Εργαστήριο, Αίθουσα: Εργαστήριο 
		if ( $typos_mathimatos == 2 AND $typos_aithousas == 2 ) {
			//do nothing
		}
		return $array_of_invalid_classroom_types;
	}
	
?>
