<?php

	include("functions.php");
	include("initial_availability_classroom.php");
	include("specific_availability_classroom.php");
	include("check_classroom_type.php");
	include("check_semester_availability.php");
	include("initial_availability_tutor.php");
	include("specific_availability_tutor.php");
	include("check_duration.php");

	

	
	$error_div_string = "";
	$sql_check_etos_pente = "SELECT parent_ID, ID_Anathesis FROM Event_Parent ORDER BY start_date,start_time ASC";
	$result_check_etos_pente = mysql_query($sql_check_etos_pente);
	$error_count = 0;

	while($row_check_etos_pente = mysql_fetch_assoc($result_check_etos_pente)){

		$ID_Event_Parent = $row_check_etos_pente['parent_ID'];
		$ID_Anathesis = $row_check_etos_pente['ID_Anathesis'];

		$sql_anathesi = "SELECT ID_Parent FROM Anathesi WHERE ID_Anathesis = '$ID_Anathesis' ";
		$result_anathesi = mysql_query($sql_anathesi);
		$row_anathesi = mysql_fetch_assoc($result_anathesi);
		$ID_Anathesi_Parent = $row_anathesi['ID_Parent'];

		$sql_anathesi_parent = "SELECT ID_OMathimatos FROM Anathesi_Parent WHERE ID_Anathesi_Parent = '$ID_Anathesi_Parent' ";
		$result_anathesi_parent = mysql_query($sql_anathesi_parent);
		$row_anathesi_parent = mysql_fetch_assoc($result_anathesi_parent);
		$ID_OMathimatos = $row_anathesi_parent['ID_OMathimatos'];

		$sql_mathima = "SELECT ID_Eksaminou FROM Mathima_Orismos WHERE ID_OMathimatos = '$ID_OMathimatos' ";
		$result_mathima = mysql_query($sql_mathima);
		$row_mathima = mysql_fetch_assoc($result_mathima);
		$ID_Eksaminou = $row_mathima['ID_Eksaminou'];

		if( $ID_Eksaminou == 9 || $ID_Eksaminou == 10 ){

			// ======================================================================//
			// 																		 //
			// 			Συνάρτηση Initial Availability Classroom 	 	 			 //
			// 																		 //
			// ======================================================================//
			
			//Διαβασε τον πίνακα που επιστρέφει η συνάρτηση και βάλτον στην μεταβλητή
			$initial_availability_classroom = initial_availability_classroom($ID_Event_Parent);
			//print_r($initial_availability_classroom);
			
			//Υπολόγισε το μήκος του πίνακα
			$array_length = count($initial_availability_classroom);

			//Αν το μήκος του ΔΕΝ ειναι μηδενικό τότε προχώρα στην κατασκευή των warnings
			if($array_length > 0){
				
				foreach ($initial_availability_classroom as $var){
					$lesson_title = $var['lesson_title'];
					$aithousa_name = $var['aithousa'];
					$error_code = "etos_pente_error_".$error_count;
					$error_div_string = $error_div_string.'<div id="'.$error_code.'"  class="group">
																<h3 style="background: rgba(220, 33, 39, 0.5); font-size: 80%;">
																	<div>
																	
																		<div align="right" width="20px"><img  onmouseover="$(this).attr(\'src\',\'styles/basic/img/delete4.png\');" onmouseout="$(this).attr(\'src\',\'styles/basic/img/delete3.png\');"onclick="event.stopPropagation(); $(this).parent().parent().parent().parent().fadeOut(500, function() { $(this).remove(); });" style="margin-right:1px"; margin-top:1px;" src="styles/basic/img/delete3.png" height="9" width="9"></div>
																		<div align="left" style="margin-bottom:10px;">'.$lesson_title.'</div>
																	
																	</div>
																</h3>
																<div>
																	<p>Η αίθουσα '.$aithousa_name.' δεν είναι διαθέσιμη τις ώρες που καταχωρήσατε.</p>
																</div>
															</div>';

				}

			}
			
			// ======================================================================//
			// 																		 //
			// 			Συνάρτηση Specific Availability Classroom 	 	 			 //
			// 																		 //
			// ======================================================================//
			
			//Διαβασε τον πίνακα που επιστρέφει η συνάρτηση και βάλτον στην μεταβλητή
			$specific_availability_classroom = specific_availability_classroom($ID_Event_Parent);
			//print_r($specific_availability_classroom);
			
			//Υπολόγισε το μήκος του πίνακα
			$array_length = count($specific_availability_classroom);

			//Αν το μήκος του ΔΕΝ ειναι μηδενικό τότε προχώρα στην κατασκευή των warnings
			if($array_length > 0){
				
				foreach ($specific_availability_classroom as $var3){
					$lesson_title = $var3['lesson_title'];
					$conflict_lesson_title = $var3['conflict_lesson_title'];
					$error_div_string = $error_div_string.'<div class="group">
																<h3 style="background: rgba(220, 33, 39, 0.5); font-size: 80%;">
																	<div>
																	
																		<div align="right" width="20px"><img  onmouseover="$(this).attr(\'src\',\'styles/basic/img/delete4.png\');" onmouseout="$(this).attr(\'src\',\'styles/basic/img/delete3.png\');"onclick="event.stopPropagation(); $(this).parent().parent().parent().parent().fadeOut(500, function() { $(this).remove(); });" style="margin-right:1px"; margin-top:1px;" src="styles/basic/img/delete3.png" height="9" width="9"></div>
																		<div align="left" style="margin-bottom:10px;">'.$lesson_title.'</div>
																	
																	</div>
																</h3>
																<div>
																	<p>Το μάθημα έχει καταχωρηθεί την <b>ίδια ώρα</b> και στην <b>ίδια αίθουσα</b> με το μάθημα:</br></br>'.$conflict_lesson_title.'</p>
																</div>
															</div>';

				}

			}
			

			// ======================================================================//
			// 																		 //
			// 					   Συνάρτηση Check Classroom Type 					 //
			// 																		 //
			// ======================================================================//
			
			//Διαβασε τον πίνακα που επιστρέφει η συνάρτηση και βάλτον στην μεταβλητή
			$check_classroom_type = check_classroom_type($ID_Event_Parent);
			// print_r($check_classroom_type);
			
			//Υπολόγισε το μήκος του πίνακα
			$array_length = count($check_classroom_type);

			//Αν το μήκος του ΔΕΝ ειναι μηδενικό τότε προχώρα στην κατασκευή των warnings
			if($array_length > 0){
				
				foreach ($check_classroom_type as $var4){
					$lesson_title = $var4['lesson_title'];
					$aithousa_name = $var4['aithousa'];
					$error_message = $var4['error_message'];
					$error_code = "etos_pente_error_".$error_count;
					$error_div_string = $error_div_string.'<div id="'.$error_code.'"  class="group">
																<h3 style="background: rgba(220, 33, 39, 0.5); font-size: 80%;">
																	<div>
																	
																		<div align="right" width="20px"><img  onmouseover="$(this).attr(\'src\',\'styles/basic/img/delete4.png\');" onmouseout="$(this).attr(\'src\',\'styles/basic/img/delete3.png\');"onclick="event.stopPropagation(); $(this).parent().parent().parent().parent().fadeOut(500, function() { $(this).remove(); });" style="margin-right:1px"; margin-top:1px;" src="styles/basic/img/delete3.png" height="9" width="9"></div>
																		<div align="left" style="margin-bottom:10px;">'.$lesson_title.'</div>
																	
																	</div>
																</h3>
																<div>
																	<p>Ο τύπος της αίθουσας '.$aithousa_name.' δεν είναι συμβατός με τον τύπο του μαθήματος. <i></br></br>'.$error_message.'</i></p>
																</div>
															</div>';

				}

			}
			
			// ======================================================================//
			// 																		 //
			//				   Συνάρτηση Check Semester Availability 				 //
			// 																		 //
			// ======================================================================//
			
			//Διαβασε τον πίνακα που επιστρέφει η συνάρτηση και βάλτον στην μεταβλητή
			$check_semester_availability = check_semester_availability($ID_Event_Parent,9,10);
			// print_r($check_classroom_type);
			
			//Υπολόγισε το μήκος του πίνακα
			$array_length = count($check_semester_availability);

			//Αν το μήκος του ΔΕΝ ειναι μηδενικό τότε προχώρα στην κατασκευή των warnings
			if($array_length > 0){
				
				foreach ($check_semester_availability as $var5){
					$lesson_title = $var5['lesson_title'];
					$conflict_lesson_title = $var5['conflict_lesson_title'];
					$error_div_string = $error_div_string.'<div class="group">
																<h3 style="background: rgba(22, 33, 39, 0.5); font-size: 80%;">
																	<div>
																	
																		<div align="right" width="20px"><img  onmouseover="$(this).attr(\'src\',\'styles/basic/img/delete4.png\');" onmouseout="$(this).attr(\'src\',\'styles/basic/img/delete3.png\');"onclick="event.stopPropagation(); $(this).parent().parent().parent().parent().fadeOut(300, function() { $(this).remove(); });" style="margin-right:1px"; margin-top:1px;" src="styles/basic/img/delete3.png" height="9" width="9"></div>
																		<div align="left" style="margin-bottom:10px;">'.$lesson_title.'</div>
																	
																	</div>
																</h3>
																<div>
																	<p>Οι ώρες του μαθήματος συμπίπτουν με τις ώρες του μαθήματος "'.$conflict_lesson_title.'"</p>
																</div>
															</div>';

				}

			}
			
			// ======================================================================//
			// 																		 //
			//				   Συνάρτηση Initial Availabiliy Tutor	 				 //
			// 																		 //
			// ======================================================================//
			
			//Διαβασε τον πίνακα που επιστρέφει η συνάρτηση και βάλτον στην μεταβλητή
			$initial_availability_tutor = initial_availability_tutor($ID_Event_Parent);
			// print_r($check_classroom_type);
			
			//Υπολόγισε το μήκος του πίνακα
			$array_length = count($initial_availability_tutor);

			//Αν το μήκος του ΔΕΝ ειναι μηδενικό τότε προχώρα στην κατασκευή των warnings
			if($array_length > 0){
				
				foreach ($initial_availability_tutor as $var6){
					$lesson_title = $var6['lesson_title'];
					$didaskon_name = $var6['didaskon_name'];
					$didaskon_id = $var6['didaskon_id'];

					$error_div_string = $error_div_string.'<div class="group">
																<h3 style="background: rgba(220, 33, 39, 0.5); font-size: 80%;">
																	<div>
																	
																		<div align="right" width="20px"><img  onmouseover="$(this).attr(\'src\',\'styles/basic/img/delete4.png\');" onmouseout="$(this).attr(\'src\',\'styles/basic/img/delete3.png\');"onclick="event.stopPropagation(); $(this).parent().parent().parent().parent().fadeOut(300, function() { $(this).remove(); });" style="margin-right:1px"; margin-top:1px;" src="styles/basic/img/delete3.png" height="9" width="9"></div>
																		<div align="left" style="margin-bottom:10px; margin-right:8px;">'.$lesson_title.'</div>
																	
																	</div>
																</h3>
																<div>
																	<p>O καθηγητής <b>'.$didaskon_name.'</b> δεν ειναι διαθέσιμος τις ώρες που καταχωρήσατε</br></br><u><a href="tutor_edit_diathesimotita.php?id='.$didaskon_id.'" target="_blank">Διαθεσιμότητα διδάσκοντα</a></u></p>
																</div>
															</div>';

				}

			}
			
			// ======================================================================//
			// 																		 //
			//				   Συνάρτηση Specific Availabiliy Tutor	 				 //
			// 																		 //
			// ======================================================================//
			
			//Διαβασε τον πίνακα που επιστρέφει η συνάρτηση και βάλτον στην μεταβλητή
			$specific_availability_tutor = specific_availability_tutor($ID_Event_Parent);
			// print_r($check_classroom_type);
			
			//Υπολόγισε το μήκος του πίνακα
			$array_length = count($specific_availability_tutor);

			//Αν το μήκος του ΔΕΝ ειναι μηδενικό τότε προχώρα στην κατασκευή των warnings
			if($array_length > 0){
				
				foreach ($specific_availability_tutor as $var7){
					$lesson_title = $var7['lesson_title'];
					$didaskon_name = $var7['didaskon_name'];
					$conflict_lesson_title = $var7['conflict_lesson_title'];

					$error_div_string = $error_div_string.'<div class="group">
																<h3 style="background: rgba(220, 33, 39, 0.5); font-size: 80%;">
																	<div>
																	
																		<div align="right" width="20px"><img  onmouseover="$(this).attr(\'src\',\'styles/basic/img/delete4.png\');" onmouseout="$(this).attr(\'src\',\'styles/basic/img/delete3.png\');"onclick="event.stopPropagation(); $(this).parent().parent().parent().parent().fadeOut(300, function() { $(this).remove(); });" style="margin-right:1px"; margin-top:1px;" src="styles/basic/img/delete3.png" height="9" width="9"></div>
																		<div align="left" style="margin-bottom:10px; margin-right:8px;">'.$lesson_title.'</div>
																	
																	</div>
																</h3>
																<div>
																	<p>O καθηγητής <b>'.$didaskon_name.'</b> δεν ειναι διαθέσιμος τις ώρες που καταχωρήσατε διότι έχει ανατεθεί τις <b>ίδιες ώρες</b> στο μάθημα:</br></br>'.$conflict_lesson_title.'.</p>
																</div>
															</div>';

				}

			}
				
			// ======================================================================//
			// 																		 //
			//				   		Συνάρτηση Check Duration	 					 //
			// 																		 //
			// ======================================================================//
			
			//Διαβασε τον πίνακα που επιστρέφει η συνάρτηση και βάλτον στην μεταβλητή
			$check_duration = check_duration($ID_Event_Parent);
			// print_r($check_classroom_type);
			
			//Υπολόγισε το μήκος του πίνακα
			$array_length = count($check_duration);

			//Αν το μήκος του ΔΕΝ ειναι μηδενικό τότε προχώρα στην κατασκευή των warnings
			if($array_length > 0){
				
				foreach ($check_duration as $var8){
					$lesson_title = $var8['lesson_title'];
					$total_lesson_hours = $var8['total_lesson_hours'];
					$entered_lesson_hours = $var8['entered_lesson_hours'];

					$error_div_string = $error_div_string.'<div class="group">
																<h3 style="background: rgba(220, 33, 39, 0.5); font-size: 80%;">
																	<div>
																	
																		<div align="right" width="20px"><img  onmouseover="$(this).attr(\'src\',\'styles/basic/img/delete4.png\');" onmouseout="$(this).attr(\'src\',\'styles/basic/img/delete3.png\');"onclick="event.stopPropagation(); $(this).parent().parent().parent().parent().fadeOut(300, function() { $(this).remove(); });" style="margin-right:1px"; margin-top:1px;" src="styles/basic/img/delete3.png" height="9" width="9"></div>
																		<div align="left" style="margin-bottom:10px; margin-right:8px;">'.$lesson_title.'</div>
																	
																	</div>
																</h3>
																<div>
																	<p>Οι ώρες που καταχωρήσατε διαφέρουν από τις απαιτούμενες ώρες του μαθήματος. </br></br> Απαιτούμενες ώρες:'.$total_lesson_hours.'</br>Καταχωρημένες ώρες: '.$entered_lesson_hours.'.</p>
																</div>
															</div>';

				}

			}
			
		}

	}

	echo $error_div_string;





?>