
<?php

	function specific_availability_tutor($ID_Event_Parent) {

		$array_of_invalid_tutor_hours = array();
		
		//Διάβασε τα στοιχεία του event
		$sql_initial_availability_classroom = "SELECT weekday, start_time, end_time, ID_Anathesis, title FROM Event_Parent WHERE parent_id = '$ID_Event_Parent'";
		$result_initial_availability_classroom = mysql_query($sql_initial_availability_classroom);
		while( $row_initial_availability_classroom = mysql_fetch_assoc($result_initial_availability_classroom) ){

			// Εκχώρησε τις τιμές 
			$weekday = $row_initial_availability_classroom['weekday'];
			$start_time = $row_initial_availability_classroom['start_time'];
			$end_time = $row_initial_availability_classroom['end_time'];
			$ID_Anathesis = $row_initial_availability_classroom['ID_Anathesis'];
			$lesson_title = $row_initial_availability_classroom['title'];

			// Μετάτρεψε τις ώρες σε διψήφιους αριθμούς 
			$clean_start_time = time_converter($start_time);
			$clean_end_time = time_converter($end_time);
			$duration = duration($clean_start_time, $clean_end_time);

			//Βρες το ID του διδάσκοντα απο την ανάθεση
			$sql_find_tutor_id = "SELECT ID_Didaskonta FROM Anathesi WHERE ID_Anathesis = '$ID_Anathesis' ";
			$result_find_tutor_id = mysql_query($sql_find_tutor_id);
			$row_find_tutor_id = mysql_fetch_assoc($result_find_tutor_id);
			$ID_Didaskonta = $row_find_tutor_id['ID_Didaskonta'];

			//Brew το ονοματεπώνυμο του διδάσκοντα
			$sql_didaskonta = "SELECT onoma, eponimo FROM Didaskon_Orismos WHERE ID_Didaskonta = '$ID_Didaskonta' ";
			$result_didaskonta = mysql_query($sql_didaskonta);
			$row_didaskonta = mysql_fetch_assoc($result_didaskonta);
			$onoma = $row_didaskonta['onoma'];
			$eponimo = $row_didaskonta['eponimo'];

			//Βρες όλες τις αναθέσεις που έχουν τον ίδιο διδάσκοντα
			$sql_find_all_anathesi = "SELECT ID_Anathesis FROM Anathesi WHERE ID_Didaskonta = '$ID_Didaskonta' ";
			$result_find_all_anathesi = mysql_query($sql_find_all_anathesi);
			
			while($row_find_all_anathesi = mysql_fetch_assoc($result_find_all_anathesi) ){

				//Βρες όλα τα events που έχουν τον ίδιο διδάσκοντα μέσω των αναθέσεων
				$ID_Anathesis_current = $row_find_all_anathesi['ID_Anathesis'];
				$sql_find_all_events = "SELECT parent_id, weekday, start_time, end_time, title FROM Event_Parent WHERE ID_Anathesis = '$ID_Anathesis_current' AND parent_id != '$ID_Event_Parent' ";
				$result_find_all_events = mysql_query($sql_find_all_events);
				
				while($row_find_all_events = mysql_fetch_assoc($result_find_all_events) ){

					// Εκχώρησε τις τιμές 
					$id_current = $row_find_all_events['parent_id'];
					$weekday_current = $row_find_all_events['weekday'];
					$start_time_current = $row_find_all_events['start_time'];
					$end_time_current = $row_find_all_events['end_time'];
					$lesson_title_current = $row_find_all_events['title'];

					// Μετάτρεψε τις ώρες σε διψήφιους αριθμούς 
					$clean_start_time_current = time_converter($start_time_current);

					$error_found = 0;

					//Ελεγξε κάθε ID_Hour_Per_Day του αρχικού μαθήματος με κάθε ID_Hour_Per_Day των άλλων μαθημάτων
					//που έχουν τον ίδιο διδάσκοντα
					for ($i=0; $i<$duration; $i++) {

						//Αν έχουμε βρει έστω και ένα error βγαίνουμε απο την επανάληψη
						if($error_found == 1) break;

						//ID_Hour_Per_Day αρχικού μαθήματος
						$ID_Hour_Per_Day = find_ID_Hour_Per_Day($weekday, $clean_start_time+$i);
						
						
						for ($j=0; $j<$duration_current; $j++) {
							
							//ID_Hour_Per_Day του κάθε άλλου μαθήματος
							$ID_Hour_Per_Day_current = find_ID_Hour_Per_Day($weekday_current, $clean_start_time_current+$j);
							
							if($ID_Hour_Per_Day == $ID_Hour_Per_Day_current){
								
								$to_be_inserted['lesson_title'] = $lesson_title;
								$to_be_inserted['conflict_lesson_title'] = $lesson_title_current;
								$to_be_inserted['didaskon_name'] = $eponimo." ".$onoma;
								
								$array_of_invalid_tutor_hours[] = $to_be_inserted;

								//Αφου βρήκαμε error αλλάζουμε την μεταβλητή error_found σε 1 και κάνουμε break
								$error_found = 1;
								break;
							}
						
						}
					
					}
				}


			}

			
			



			

			return $array_of_invalid_tutor_hours;

		}

	}

?>