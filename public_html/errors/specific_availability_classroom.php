
<?php
	
	function specific_availability_classroom($ID_Event_Parent) {

		$array_of_specific_invalid_hours = array();
		$Final_Aithousa_Name = "";

		$sql_specific_availability_classroom = "SELECT weekday, start_time, end_time, ID_Aithousas, title FROM Event_Parent WHERE parent_id = '$ID_Event_Parent'";
		$result_specific_availability_classroom = mysql_query($sql_specific_availability_classroom);
		$row_specific_availability_classroom = mysql_fetch_assoc($result_specific_availability_classroom);

		// Εκχώρησε τις τιμές 
		$weekday = $row_specific_availability_classroom['weekday'];
		$start_time = $row_specific_availability_classroom['start_time'];
		$end_time = $row_specific_availability_classroom['end_time'];
		$ID_Aithousas = $row_specific_availability_classroom['ID_Aithousas'];
		$lesson_title = $row_specific_availability_classroom['title'];

		// Μετάτρεψε τις ώρες σε διψήφιους αριθμούς 
		$clean_start_time = time_converter($start_time);

		// Βρες σε ποιο ID_Hour_Per_Day αντιστοιχούν οι ώρες αυτού του μαθήματος 
		

			$sql_other_events = "SELECT parent_id, weekday, start_time, end_time, ID_Aithousas, title FROM Event_Parent WHERE ID_Aithousas = '$ID_Aithousas' AND parent_id != '$ID_Event_Parent'";

			$result_other_events = mysql_query($sql_other_events);
			while( $row_other_events = mysql_fetch_assoc($result_other_events) ){

				// Εκχώρησε τις τιμές 
				$id_current = $row_other_events['parent_id'];
				$weekday_current = $row_other_events['weekday'];
				$start_time_current = $row_other_events['start_time'];
				$end_time_current = $row_other_events['end_time'];
				$ID_Aithousas_current = $row_other_events['ID_Aithousas'];
				$lesson_title_current = $row_other_events['title'];

				// Μετάτρεψε τις ώρες σε διψήφιους αριθμούς 
				$clean_start_time_current = time_converter($start_time_current);
				$clean_end_time_current = time_converter($end_time_current);
				$duration_current = duration($clean_start_time_current, $clean_end_time_current);

				$error_found = 0;

				//Ελεγξε κάθε ID_Hour_Per_Day του αρχικού μαθήματος με κάθε ID_Hour_Per_Day των άλλων μαθημάτων
				for ($i=0; $i<$duration; $i++) {

					//Αν έχουμε βρει έστω και ένα error βγαίνουμε απο την επανάληψη
					if($error_found == 1) break;

					//ID_Hour_Per_Day αρχικού μαθήματος
					$ID_Hour_Per_Day = find_ID_Hour_Per_Day($weekday, $clean_start_time+$i);
					
					
					for ($j=0; $j<$duration_current; $j++) {
						
						//ID_Hour_Per_Day του κάθε άλλου μαθήματος
						$ID_Hour_Per_Day_current = find_ID_Hour_Per_Day($weekday_current, $clean_start_time_current+$j);
						
						if($ID_Hour_Per_Day == $ID_Hour_Per_Day_current){
							
							$to_be_inserted['lesson_title'] = $lesson_title;
							$to_be_inserted['conflict_lesson_title'] = $lesson_title_current;
							
							$array_of_specific_invalid_hours[] = $to_be_inserted;

							//Αφου βρήκαμε error αλλάζουμε την μεταβλητή error_found σε 1 και κάνουμε break
							$error_found = 1;
							break;
						}
					
					}
				
				}

			}

		return $array_of_specific_invalid_hours;
	}


?>
