<?php
		
	function initial_availability_tutor($ID_Event_Parent) {

		$array_of_invalid_tutor_hours = array();
		$hpd_slot = array();

		$sql_initial_availability_classroom = "SELECT weekday, start_time, end_time, ID_Aithousas, ID_Anathesis, title FROM Event_Parent WHERE parent_id = '$ID_Event_Parent'";
		$result_initial_availability_classroom = mysql_query($sql_initial_availability_classroom);
		$row_initial_availability_classroom = mysql_fetch_assoc($result_initial_availability_classroom);

		// Εκχώρησε τις τιμές 
		$weekday = $row_initial_availability_classroom['weekday'];
		$start_time = $row_initial_availability_classroom['start_time'];
		$end_time = $row_initial_availability_classroom['end_time'];
		$ID_Aithousas = $row_initial_availability_classroom['ID_Aithousas'];
		$ID_Anathesis = $row_initial_availability_classroom['ID_Anathesis'];

		$lesson_title = $row_initial_availability_classroom['title'];

		// Μετάτρεψε τις ώρες σε διψήφιους αριθμούς 
		$clean_start_time = time_converter($start_time);
		$clean_end_time = time_converter($end_time);
		$duration = duration($clean_start_time, $clean_end_time);

		//Τράβα το ID_Didaskonta
		$sql_anathesi = "SELECT ID_Didaskonta FROM Anathesi WHERE ID_Anathesis = '$ID_Anathesis' ";
		$result_anathesi = mysql_query($sql_anathesi);
		$row_anathesi = mysql_fetch_assoc($result_anathesi);
		$ID_Didaskonta = $row_anathesi['ID_Didaskonta'];

		//Τράβα το ονοματεπώνυμο του διδάσκοντα
		$sql_didaskonta = "SELECT onoma, eponimo FROM Didaskon_Orismos WHERE ID_Didaskonta = '$ID_Didaskonta' ";
		$result_didaskonta = mysql_query($sql_didaskonta);
		$row_didaskonta = mysql_fetch_assoc($result_didaskonta);
		$onoma = $row_didaskonta['onoma'];
		$eponimo = $row_didaskonta['eponimo'];


		// Για την εύρεση των μαθημάτων στα οποία δεν ειναι διαθέσιμος ο διδάσκων θα ακολουθήσουμε τον εξής αλγόριθμο:
		// Αρχικά "φτιαχνουμε" έναν πίνακα (δημιουργείται δυναμικά) με τοσες στήλες όσο η διάρκεια του μαθήματος σε ώρες (duration), 
		// και κάθε στήλη την κάνουμε ίση με 0. Υστερα τρέχουμε μια επανάληψη η οποία ελέγχει κάθε Hour_Per_Day του μαθήματος με
		// κάθε Hour_Per_Day που είναι διαθέσιμος ο διδάσκων. Για κάθε ισότητα -που σημαίνει διαθεσιμότητα καθηγητή για το συγκεκριμένο
		// μονόωρο- αλλάζουμε την ανάλογη στήλη του hpd_slot σε 1. Αφού τελειώσει η επανάληψη ελέγχουμε τον πίνακα hpd_slot. Αν όλες οι 
		// στήλες είναι ίσες με 1, σημαίνει ότι για κάθε μονόωρο του μαθήματος βρήκαμε καταχώρηση στην διαθεσιμότητα του διδάσκοντος.
		// Αν έχει έστω και ένα μηδενικό μας αρκεί για να συμπεράνουμε οτι στις ώρες που καταχωρήθηκε το μάθημα (ακόμα και μόνο σε μια) 
		// ο καθηγητής δεν ειναι διαθέσιμος και να εμφανίσουμε το αντίστοιχο μήνυμα σφάλματος.

		//Αρχικοποίηση πίνακα hpd_slot
		for ($i=0; $i<$duration; $i++) {
			$hpd_slot[$i] = 0;  //hour per day slot
		}

		//Πολλαπλές επαναληψεις για τον έλεγχο της διαθεσιμότητας του καθηγητή
		$sql_diathesimotita = "SELECT start, end FROM Didaskon_Arxiki_Diathesimotita WHERE ID_Didaskonta = '$ID_Didaskonta' ";
		$result_diathesimotita = mysql_query($sql_diathesimotita);
		
		if(mysql_num_rows($result_diathesimotita)>0){
			while($row_diathesimotita = mysql_fetch_assoc($result_diathesimotita) ) {

				$start_didaskon = $row_diathesimotita['start'];
				$end_didaskon = $row_diathesimotita['end'];

				
				for ($i=0; $i<$duration; $i++) {

					for ($j=$start_didaskon; $j<$end_didaskon; $j++) {

						if($ID_Hour_Per_Day == $j){

							$hpd_slot[$i] = 1;
							
						}

					}

				}

			}
		

			//Τελικός έλεγχος πίνακα hpd_slot. Στο πρώτο μηδενικό που θα βρούμε σταματάμε και εμφανιζουμε μήνυμα σφάλματος
			for ($i=0; $i<$duration; $i++) {
				
				if($hpd_slot[$i] == 0){
					$to_be_inserted['lesson_title'] = $lesson_title;
					$to_be_inserted['didaskon_name'] = $eponimo." ".$onoma;
					$to_be_inserted['didaskon_id'] = $ID_Didaskonta;

					$array_of_invalid_tutor_hours[] = $to_be_inserted;
					//echo "Error";
					break;
				}
			}


		}

		/*$sql_anathesi_parent = "SELECT ID_OMathimatos FROM Anathesi_Parent WHERE ID_Anathesi_Parent = '$ID_Anathesi_Parent' ";
		$result_anathesi_parent = mysql_query($sql_anathesi_parent);
		$row_anathesi_parent = mysql_fetch_assoc($result_anathesi_parent);
		$ID_OMathimatos = $row_anathesi_parent['ID_OMathimatos'];

		$sql_mathima = "SELECT ID_Eksaminou FROM Mathima_Orismos WHERE ID_OMathimatos = '$ID_OMathimatos' ";
		$result_mathima = mysql_query($sql_mathima);
		$row_mathima = mysql_fetch_assoc($result_mathima);
		$ID_Eksaminou = $row_mathima['ID_Eksaminou'];
		*/

		return $array_of_invalid_tutor_hours;

	}

?>