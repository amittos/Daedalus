<?php
	session_start();
	include("database.php"); 

	//======================================================================//
	//======================================================================//
	//======================================================================//
  	//																		//
  	//					Διάβασμα στοιχείων για κάθε αίθουσα					//
	//																		//
	//======================================================================//
	//======================================================================//
	//======================================================================//

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα Σοφούλης				//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας Σοφούλης 
	$sql_sofoulis = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '1' ";							
	$result_sofoulis = mysql_query($sql_sofoulis);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_sofoulis = array(array()); 
	
	while ($row_sofoulis = mysql_fetch_assoc($result_sofoulis)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day = $row_sofoulis['ID_Hour_Per_Day'];
		$diathesimotita = $row_sofoulis['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_sofoulis_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day' ";
		$result_sofoulis_2 = mysql_query($sql_sofoulis_2);
		$row_sofoulis_2 = mysql_fetch_assoc($result_sofoulis_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour = $row_sofoulis_2['ID_Hour'];
	    $day = $row_sofoulis_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_sofoulis[$day][$hour-9] = $diathesimotita; 		
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα No.1 					//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας No.1 
	$sql_no1 = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '2' ";							
	$result_no1 = mysql_query($sql_no1);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_no1 = array(array()); 
	
	while ($row_no1 = mysql_fetch_assoc($result_no1)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_no1 = $row_no1['ID_Hour_Per_Day'];
		$diathesimotita_no1 = $row_no1['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_no1_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_no1' ";
		$result_no1_2 = mysql_query($sql_no1_2);
		$row_no1_2 = mysql_fetch_assoc($result_no1_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_no1 = $row_no1_2['ID_Hour'];
	    $day_no1 = $row_no1_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_no1[$day_no1][$hour_no1-9] = $diathesimotita_no1;	
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα No.3 					//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας No.3 
	$sql_no3 = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '3' ";							
	$result_no3 = mysql_query($sql_no3);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_no3 = array(array()); 
	
	while ($row_no3 = mysql_fetch_assoc($result_no3)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_no3 = $row_no3['ID_Hour_Per_Day'];
		$diathesimotita_no3 = $row_no3['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_no3_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_no3' ";
		$result_no3_2 = mysql_query($sql_no3_2);
		$row_no3_2 = mysql_fetch_assoc($result_no3_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_no3 = $row_no3_2['ID_Hour'];
	    $day_no3 = $row_no3_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_no3[$day_no3][$hour_no3-9] = $diathesimotita_no3;		
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα Δήμητρα 				//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας dimitra 
	$sql_dimitra = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '4' ";							
	$result_dimitra = mysql_query($sql_dimitra);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_dimitra = array(array()); 
	
	while ($row_dimitra = mysql_fetch_assoc($result_dimitra)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_dimitra = $row_dimitra['ID_Hour_Per_Day'];
		$diathesimotita_dimitra = $row_dimitra['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_dimitra_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_dimitra' ";
		$result_dimitra_2 = mysql_query($sql_dimitra_2);
		$row_dimitra_2 = mysql_fetch_assoc($result_dimitra_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_dimitra = $row_dimitra_2['ID_Hour'];
	    $day_dimitra = $row_dimitra_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_dimitra[$day_dimitra][$hour_dimitra-9] = $diathesimotita_dimitra;		
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα Λυτώ 					//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας lito 
	$sql_lito = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '5' ";							
	$result_lito = mysql_query($sql_lito);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_lito = array(array()); 
	
	while ($row_lito = mysql_fetch_assoc($result_lito)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_lito = $row_lito['ID_Hour_Per_Day'];
		$diathesimotita_lito = $row_lito['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_lito_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_lito' ";
		$result_lito_2 = mysql_query($sql_lito_2);
		$row_lito_2 = mysql_fetch_assoc($result_lito_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_lito = $row_lito_2['ID_Hour'];
	    $day_lito = $row_lito_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_lito[$day_lito][$hour_lito-9] = $diathesimotita_lito;		
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα Χαριτίνη 				//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας xaritini 
	$sql_xaritini = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '6' ";							
	$result_xaritini = mysql_query($sql_xaritini);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_xaritini = array(array()); 
	
	while ($row_xaritini = mysql_fetch_assoc($result_xaritini)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_xaritini = $row_xaritini['ID_Hour_Per_Day'];
		$diathesimotita_xaritini = $row_xaritini['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_xaritini_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_xaritini' ";
		$result_xaritini_2 = mysql_query($sql_xaritini_2);
		$row_xaritini_2 = mysql_fetch_assoc($result_xaritini_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_xaritini = $row_xaritini_2['ID_Hour'];
	    $day_xaritini = $row_xaritini_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_xaritini[$day_xaritini][$hour_xaritini-9] = $diathesimotita_xaritini;		
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα Μυρτώ					//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας mirto 
	$sql_mirto = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '7' ";							
	$result_mirto = mysql_query($sql_mirto);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_mirto = array(array()); 
	
	while ($row_mirto = mysql_fetch_assoc($result_mirto)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_mirto = $row_mirto['ID_Hour_Per_Day'];
		$diathesimotita_mirto = $row_mirto['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_mirto_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_mirto' ";
		$result_mirto_2 = mysql_query($sql_mirto_2);
		$row_mirto_2 = mysql_fetch_assoc($result_mirto_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_mirto = $row_mirto_2['ID_Hour'];
	    $day_mirto = $row_mirto_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_mirto[$day_mirto][$hour_mirto-9] = $diathesimotita_mirto;		
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα Δόρυσσα 				//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας dorisa 
	$sql_dorisa = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '8' ";							
	$result_dorisa = mysql_query($sql_dorisa);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_dorisa = array(array()); 
	
	while ($row_dorisa = mysql_fetch_assoc($result_dorisa)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_dorisa = $row_dorisa['ID_Hour_Per_Day'];
		$diathesimotita_dorisa = $row_dorisa['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_dorisa_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_dorisa' ";
		$result_dorisa_2 = mysql_query($sql_dorisa_2);
		$row_dorisa_2 = mysql_fetch_assoc($result_dorisa_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_dorisa = $row_dorisa_2['ID_Hour'];
	    $day_dorisa = $row_dorisa_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_dorisa[$day_dorisa][$hour_dorisa-9] = $diathesimotita_dorisa;		
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα Νέο Σχολικό 			//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας neosxoliko 
	$sql_neosxoliko = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '9' ";							
	$result_neosxoliko = mysql_query($sql_neosxoliko);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_neosxoliko = array(array()); 
	
	while ($row_neosxoliko = mysql_fetch_assoc($result_neosxoliko)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_neosxoliko = $row_neosxoliko['ID_Hour_Per_Day'];
		$diathesimotita_neosxoliko = $row_neosxoliko['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_neosxoliko_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_neosxoliko' ";
		$result_neosxoliko_2 = mysql_query($sql_neosxoliko_2);
		$row_neosxoliko_2 = mysql_fetch_assoc($result_neosxoliko_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_neosxoliko = $row_neosxoliko_2['ID_Hour'];
	    $day_neosxoliko = $row_neosxoliko_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_neosxoliko[$day_neosxoliko][$hour_neosxoliko-9] = $diathesimotita_neosxoliko;		
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα Ηλέκτρα				//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας ilektra 
	$sql_ilektra = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '10' ";							
	$result_ilektra = mysql_query($sql_ilektra);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_ilektra = array(array()); 
	
	while ($row_ilektra = mysql_fetch_assoc($result_ilektra)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_ilektra = $row_ilektra['ID_Hour_Per_Day'];
		$diathesimotita_ilektra = $row_ilektra['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_ilektra_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_ilektra' ";
		$result_ilektra_2 = mysql_query($sql_ilektra_2);
		$row_ilektra_2 = mysql_fetch_assoc($result_ilektra_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_ilektra = $row_ilektra_2['ID_Hour'];
	    $day_ilektra = $row_ilektra_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_ilektra[$day_ilektra][$hour_ilektra-9] = $diathesimotita_ilektra;		
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα Αλκμήνη 				//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας alkmini 
	$sql_alkmini = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '11' ";							
	$result_alkmini = mysql_query($sql_alkmini);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_alkmini = array(array()); 
	
	while ($row_alkmini = mysql_fetch_assoc($result_alkmini)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_alkmini = $row_alkmini['ID_Hour_Per_Day'];
		$diathesimotita_alkmini = $row_alkmini['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_alkmini_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_alkmini' ";
		$result_alkmini_2 = mysql_query($sql_alkmini_2);
		$row_alkmini_2 = mysql_fetch_assoc($result_alkmini_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_alkmini = $row_alkmini_2['ID_Hour'];
	    $day_alkmini = $row_alkmini_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_alkmini[$day_alkmini][$hour_alkmini-9] = $diathesimotita_alkmini;		
	}

	//======================================================================//
	//																		//
	//				Διάβασμα στοιχείων για τον πίνακα Φαίδρα				//
	//																		//
	//======================================================================//

	//Από τον πίνακα Αρχική Διαθεσιμότητα πάρε για τα κουτιά (ώρες ανά ημέρα) τη διαθεσιμότητα της αίθουσας faidra 
	$sql_faidra = "SELECT ID_Hour_Per_Day, diathesimotita FROM Aithousa_Arxiki_Diathesimotita WHERE ID_Aithousas = '12' ";							
	$result_faidra = mysql_query($sql_faidra);
	
	//Ορίζουμε έναν πίνακα 2 διαστάσεων
	$array_faidra = array(array()); 
	
	while ($row_faidra = mysql_fetch_assoc($result_faidra)) { 
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$ID_Hour_Per_Day_faidra = $row_faidra['ID_Hour_Per_Day'];
		$diathesimotita_faidra = $row_faidra['diathesimotita'];
		
		//Για κάθε ID_Hour_Per_Day βρες σε ποια μέρα και ώρα αντιστοιχεί 
		$sql_faidra_2 = "SELECT ID_Day, ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$ID_Hour_Per_Day_faidra' ";
		$result_faidra_2 = mysql_query($sql_faidra_2);
		$row_faidra_2 = mysql_fetch_assoc($result_faidra_2);
		
		//Βάλε τις τιμές της βάσης στις μεταβλητές 
		$hour_faidra = $row_faidra_2['ID_Hour'];
	    $day_faidra = $row_faidra_2['ID_Day'];
		
		//Φτιάχνουμε ένα grid με διαθεσιμότητες της εβδομάδας
		$array_faidra[$day_faidra][$hour_faidra-9] = $diathesimotita_faidra;		
	}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" lang="el" xml:lang="en">

<head>

	<!--
	//======================================================================//
	//																		//
	//								Libraries								//
	//																		//
	//======================================================================//
	-->

	<title>Διαθεσιμότητα Αιθουσών - Δαίδαλος </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Language" content="el">

	<!--<link rel="stylesheet" href="styles/basic/input.css" type="text/css" media="screen"/>--> <!-- Αρχικό CSS - Υλοποίηση Κυριάκου -->
	<link rel="stylesheet" href="styles/basic/style.css"> <!-- New CSS of Doom for the Header -->
	<link rel="shortcut icon" href="https://pithos.grnet.gr/pithos/rest/icsd08158@aegean.gr/files/favicon.icon" />
	
	<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">--> <!-- Αυθεντική βιβλιοθήκη JQuery στην οποία βασιστήκαμε -->
	<link rel="stylesheet" href="styles/basic/jquery-ui.css"> <!-- JQuery βιβλιοθήκη, με πειραγμένα τα Tabs -->
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<link rel="stylesheet" href="/resources/demos/style.css">
	<style type="text/css">
	.modal {
	    display:    none;
	    position:   fixed;
	    z-index:    1000;
	    top:        0;
	    left:       0;
	    padding-top: 150px;
	    height:     100%;
	    width:      100%;
	    background: rgba( 255, 255, 255, .8 ) 
	                url('img/ajax-loader.gif') 
	                50% 50% 
	                no-repeat;
	}

	/* When the body has the loading class, we turn
	   the scrollbar off with overflow:hidden */
	body.loading {
	    overflow: hidden;   
	}

	/* Anytime the body has the loading class, our
	   modal element will be visible */
	body.loading .modal {
	    display: block;
	}
	</style>
	<script>




		//======================================================================//
		//======================================================================//
		//======================================================================//
	  	//																		//
	  	//					  Απεικόνιση για κάθε αίθουσα						//
		//																		//
		//======================================================================//
		//======================================================================//
		//======================================================================//

	 	$(function() {
		  	
			<?php
				include("aithouses/sofoulis_buttonset.php");
				include("aithouses/no1_buttonset.php");
				include("aithouses/no3_buttonset.php");
				include("aithouses/dimitra_buttonset.php");
				include("aithouses/lito_buttonset.php");
				include("aithouses/xaritini_buttonset.php");
				include("aithouses/mirto_buttonset.php");
				include("aithouses/dorisa_buttonset.php");
				include("aithouses/neosxoliko_buttonset.php");
				include("aithouses/ilektra_buttonset.php");
				include("aithouses/alkmini_buttonset.php");
				include("aithouses/faidra_buttonset.php");
			?>

			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Σοφούλης					//
			//																		//
			//======================================================================//
		  	
		  	//Περνάμε τον πίνακα $array_sofoulis της php στην Javascript 
			var array_sofoulis = new Array();
			array_sofoulis = <?php echo json_encode($array_sofoulis); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days = i;
					var hours = j+9;
					var argument = array_sofoulis[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument == 1){
						//alert("argument == 0");
						$( "#sofoulis :input[name= "+days+"][value= "+hours+"]" ).attr('checked',true).button('refresh');
					}
					if(argument == 0){
						//alert("argument == 0");
						$( "#sofoulis :input[name= "+days+"][value= "+hours+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}


			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Νο.1 						//
			//																		//
			//======================================================================//
		  	
		  	//Περνάμε τον πίνακα $array_no1 της php στην Javascript 
			var array_no1 = new Array();
			array_no1 = <?php echo json_encode($array_no1); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_no1 = i;
					var hours_no1 = j+9;
					var argument_no1 = array_no1[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_no1 == 1){
						//alert("argument == 0");
						$( "#no1 :input[name= "+days_no1+"][value= "+hours_no1+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_no1 == 0){
						//alert("argument == 0");
						$( "#no1 :input[name= "+days_no1+"][value= "+hours_no1+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}

			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Νο.3 						//
			//																		//
			//======================================================================//

			//Περνάμε τον πίνακα $array_no3 της php στην Javascript 
			var array_no3 = new Array();
			array_no3 = <?php echo json_encode($array_no3); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_no3 = i;
					var hours_no3 = j+9;
					var argument_no3 = array_no3[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_no3 == 1){
						//alert("argument == 0");
						$( "#no3 :input[name= "+days_no3+"][value= "+hours_no3+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_no3 == 0){
						//alert("argument == 0");
						$( "#no3 :input[name= "+days_no3+"][value= "+hours_no3+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}

			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Δήμητρα					//
			//																		//
			//======================================================================//

			//Περνάμε τον πίνακα $array_dimitra της php στην Javascript 
			var array_dimitra = new Array();
			array_dimitra = <?php echo json_encode($array_dimitra); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_dimitra = i;
					var hours_dimitra = j+9;
					var argument_dimitra = array_dimitra[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_dimitra == 1){
						//alert("argument == 0");
						$( "#dimitra :input[name= "+days_dimitra+"][value= "+hours_dimitra+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_dimitra == 0){
						//alert("argument == 0");
						$( "#dimitra :input[name= "+days_dimitra+"][value= "+hours_dimitra+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}


			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Λυτώ						//
			//																		//
			//======================================================================//

			//Περνάμε τον πίνακα $array_lito της php στην Javascript 
			var array_lito = new Array();
			array_lito = <?php echo json_encode($array_lito); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_lito = i;
					var hours_lito = j+9;
					var argument_lito = array_lito[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_lito == 1){
						//alert("argument == 0");
						$( "#lito :input[name= "+days_lito+"][value= "+hours_lito+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_lito == 0){
						//alert("argument == 0");
						$( "#lito :input[name= "+days_lito+"][value= "+hours_lito+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}

			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Χαριτίνη 					//
			//																		//
			//======================================================================//

			//Περνάμε τον πίνακα $array_xaritini της php στην Javascript 
			var array_xaritini = new Array();
			array_xaritini = <?php echo json_encode($array_xaritini); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_xaritini = i;
					var hours_xaritini = j+9;
					var argument_xaritini = array_xaritini[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_xaritini == 1){
						//alert("argument == 0");
						$( "#xaritini :input[name= "+days_xaritini+"][value= "+hours_xaritini+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_xaritini == 0){
						//alert("argument == 0");
						$( "#xaritini :input[name= "+days_xaritini+"][value= "+hours_xaritini+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}

			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Μυρτώ						//
			//																		//
			//======================================================================//

			//Περνάμε τον πίνακα $array_mirto της php στην Javascript 
			var array_mirto = new Array();
			array_mirto = <?php echo json_encode($array_mirto); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_mirto = i;
					var hours_mirto = j+9;
					var argument_mirto = array_mirto[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_mirto == 1){
						//alert("argument == 0");
						$( "#mirto :input[name= "+days_mirto+"][value= "+hours_mirto+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_mirto == 0){
						//alert("argument == 0");
						$( "#mirto :input[name= "+days_mirto+"][value= "+hours_mirto+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}

			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Δόρυσσα					//
			//																		//
			//======================================================================//

			//Περνάμε τον πίνακα $array_dorisa της php στην Javascript 
			var array_dorisa = new Array();
			array_dorisa = <?php echo json_encode($array_dorisa); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_dorisa = i;
					var hours_dorisa = j+9;
					var argument_dorisa = array_dorisa[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_dorisa == 1){
						//alert("argument == 0");
						$( "#dorisa :input[name= "+days_dorisa+"][value= "+hours_dorisa+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_dorisa == 0){
						//alert("argument == 0");
						$( "#dorisa :input[name= "+days_dorisa+"][value= "+hours_dorisa+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}

			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Νέο Σχολικό				//
			//																		//
			//======================================================================//

			//Περνάμε τον πίνακα $array_neosxoliko της php στην Javascript 
			var array_neosxoliko = new Array();
			array_neosxoliko = <?php echo json_encode($array_neosxoliko); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_neosxoliko = i;
					var hours_neosxoliko = j+9;
					var argument_neosxoliko = array_neosxoliko[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_neosxoliko == 1){
						//alert("argument == 0");
						$( "#neosxoliko :input[name= "+days_neosxoliko+"][value= "+hours_neosxoliko+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_neosxoliko == 0){
						//alert("argument == 0");
						$( "#neosxoliko :input[name= "+days_neosxoliko+"][value= "+hours_neosxoliko+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}

			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Ηλέκτρα					//
			//																		//
			//======================================================================//

			//Περνάμε τον πίνακα $array_ilektra της php στην Javascript 
			var array_ilektra = new Array();
			array_ilektra = <?php echo json_encode($array_ilektra); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_ilektra = i;
					var hours_ilektra = j+9;
					var argument_ilektra = array_ilektra[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_ilektra == 1){
						//alert("argument == 0");
						$( "#ilektra :input[name= "+days_ilektra+"][value= "+hours_ilektra+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_ilektra == 0){
						//alert("argument == 0");
						$( "#ilektra :input[name= "+days_ilektra+"][value= "+hours_ilektra+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}

			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Αλκμήνη 					//
			//																		//
			//======================================================================//

			//Περνάμε τον πίνακα $array_alkmini της php στην Javascript 
			var array_alkmini = new Array();
			array_alkmini = <?php echo json_encode($array_alkmini); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_alkmini = i;
					var hours_alkmini = j+9;
					var argument_alkmini = array_alkmini[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_alkmini == 1){
						//alert("argument == 0");
						$( "#alkmini :input[name= "+days_alkmini+"][value= "+hours_alkmini+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_alkmini == 0){
						//alert("argument == 0");
						$( "#alkmini :input[name= "+days_alkmini+"][value= "+hours_alkmini+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}

			//======================================================================//
		  	//																		//
		  	//					Απεικόνισιση της αίθουσας Φαίδρα					//
			//																		//
			//======================================================================//

			//Περνάμε τον πίνακα $array_faidra της php στην Javascript 
			var array_faidra = new Array();
			array_faidra = <?php echo json_encode($array_faidra); ?>
			
			//Διάβασε την βάση και παρουσίασέ την 
			for(var i=0; i<7; i++){
				for(var j=0; j<=11; j++){
					
					var days_faidra = i;
					var hours_faidra = j+9;
					var argument_faidra = array_faidra[i][j];
					//document.write(days+" "+hours+" "+array_sofoulis[i][j]+"</br>"); //Debugging

					//Για κάθε κελί της εβδομάδας, δες τι είναι και άλλαξε το button αναλόγως 
					if(argument_faidra == 1){
						//alert("argument == 0");
						$( "#faidra :input[name= "+days_faidra+"][value= "+hours_faidra+"]" ).attr('checked',true).button('refresh');
					}
					if(argument_faidra == 0){
						//alert("argument == 0");
						$( "#faidra :input[name= "+days_faidra+"][value= "+hours_faidra+"]" ).attr('checked',false).button('refresh');
					}
					
				}
				
			}



			
		  	});

			//======================================================================//
			//======================================================================//
			//======================================================================//
		  	//																		//
		  	//	Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη για κάθε αίθουσα		//
			//																		//
			//======================================================================//
			//======================================================================//
			//======================================================================//




		  	//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Σοφούλης)		//
			//																		//
			//======================================================================//

			function onClick(element,aithousa){
			
				var day = element.name;
				var hour = element.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element.value == 0){
					for(var h = 9; h<=20; h++){
						$( "#update-days" ).val(day);
						$( "#update-hours" ).val(h);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element.checked == 1){ 
				
					$( "#update-days" ).val(day);
					$( "#update-hours" ).val(hour);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa);

				}

				else{
				
					$( "#update-days" ).val(day);
					$( "#update-hours" ).val(hour);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Νο.1)			//
			//																		//
			//======================================================================//

			function onClick_no1(element_no1,aithousa_no1){
			
				var day_no1 = element_no1.name;
				var hour_no1 = element_no1.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_no1.value == 0){
					for(var h_no1 = 9; h_no1<=20; h_no1++){
						$( "#update-days" ).val(day_no1);
						$( "#update-hours" ).val(h_no1);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_no1);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_no1.checked == 1){ 
				
					$( "#update-days" ).val(day_no1);
					$( "#update-hours" ).val(hour_no1);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_no1);

				}

				else{
				
					$( "#update-days" ).val(day_no1);
					$( "#update-hours" ).val(hour_no1);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_no1);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Νο.3)			//
			//																		//
			//======================================================================//

			function onClick_no3(element_no3,aithousa_no3){
			
				var day_no3 = element_no3.name;
				var hour_no3 = element_no3.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_no3.value == 0){
					for(var h_no3 = 9; h_no3<=20; h_no3++){
						$( "#update-days" ).val(day_no3);
						$( "#update-hours" ).val(h_no3);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_no3);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_no3.checked == 1){ 
				
					$( "#update-days" ).val(day_no3);
					$( "#update-hours" ).val(hour_no3);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_no3);

				}

				else{
				
					$( "#update-days" ).val(day_no3);
					$( "#update-hours" ).val(hour_no3);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_no3);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Δήμητρα)		//
			//																		//
			//======================================================================//

			function onClick_dimitra(element_dimitra,aithousa_dimitra){
			
				var day_dimitra = element_dimitra.name;
				var hour_dimitra = element_dimitra.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_dimitra.value == 0){
					for(var h_dimitra = 9; h_dimitra<=20; h_dimitra++){
						$( "#update-days" ).val(day_dimitra);
						$( "#update-hours" ).val(h_dimitra);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_dimitra);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_dimitra.checked == 1){ 
				
					$( "#update-days" ).val(day_dimitra);
					$( "#update-hours" ).val(hour_dimitra);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_dimitra);

				}

				else{
				
					$( "#update-days" ).val(day_dimitra);
					$( "#update-hours" ).val(hour_dimitra);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_dimitra);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Λυτώ)			//
			//																		//
			//======================================================================//

			function onClick_lito(element_lito,aithousa_lito){
			
				var day_lito = element_lito.name;
				var hour_lito = element_lito.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_lito.value == 0){
					for(var h_lito = 9; h_lito<=20; h_lito++){
						$( "#update-days" ).val(day_lito);
						$( "#update-hours" ).val(h_lito);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_lito);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_lito.checked == 1){ 
				
					$( "#update-days" ).val(day_lito);
					$( "#update-hours" ).val(hour_lito);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_lito);

				}

				else{
				
					$( "#update-days" ).val(day_lito);
					$( "#update-hours" ).val(hour_lito);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_lito);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Χαριτίνη)		//
			//																		//
			//======================================================================//

			function onClick_xaritini(element_xaritini,aithousa_xaritini){
			
				var day_xaritini = element_xaritini.name;
				var hour_xaritini = element_xaritini.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_xaritini.value == 0){
					for(var h_xaritini = 9; h_xaritini<=20; h_xaritini++){
						$( "#update-days" ).val(day_xaritini);
						$( "#update-hours" ).val(h_xaritini);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_xaritini);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_xaritini.checked == 1){ 
				
					$( "#update-days" ).val(day_xaritini);
					$( "#update-hours" ).val(hour_xaritini);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_xaritini);

				}

				else{
				
					$( "#update-days" ).val(day_xaritini);
					$( "#update-hours" ).val(hour_xaritini);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_xaritini);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Μυρτώ)			//
			//																		//
			//======================================================================//

			function onClick_mirto(element_mirto,aithousa_mirto){
			
				var day_mirto = element_mirto.name;
				var hour_mirto = element_mirto.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_mirto.value == 0){
					for(var h_mirto = 9; h_mirto<=20; h_mirto++){
						$( "#update-days" ).val(day_mirto);
						$( "#update-hours" ).val(h_mirto);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_mirto);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_mirto.checked == 1){ 
				
					$( "#update-days" ).val(day_mirto);
					$( "#update-hours" ).val(hour_mirto);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_mirto);

				}

				else{
				
					$( "#update-days" ).val(day_mirto);
					$( "#update-hours" ).val(hour_mirto);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_mirto);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Δόρυσσα)		//
			//																		//
			//======================================================================//

			function onClick_dorisa(element_dorisa,aithousa_dorisa){
			
				var day_dorisa = element_dorisa.name;
				var hour_dorisa = element_dorisa.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_dorisa.value == 0){
					for(var h_dorisa = 9; h_dorisa<=20; h_dorisa++){
						$( "#update-days" ).val(day_dorisa);
						$( "#update-hours" ).val(h_dorisa);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_dorisa);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_dorisa.checked == 1){ 
				
					$( "#update-days" ).val(day_dorisa);
					$( "#update-hours" ).val(hour_dorisa);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_dorisa);

				}

				else{
				
					$( "#update-days" ).val(day_dorisa);
					$( "#update-hours" ).val(hour_dorisa);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_dorisa);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Νέο Σχολικό)	//
			//																		//
			//======================================================================//

			function onClick_neosxoliko(element_neosxoliko,aithousa_neosxoliko){
			
				var day_neosxoliko = element_neosxoliko.name;
				var hour_neosxoliko = element_neosxoliko.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_neosxoliko.value == 0){
					for(var h_neosxoliko = 9; h_neosxoliko<=20; h_neosxoliko++){
						$( "#update-days" ).val(day_neosxoliko);
						$( "#update-hours" ).val(h_neosxoliko);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_neosxoliko);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_neosxoliko.checked == 1){ 
				
					$( "#update-days" ).val(day_neosxoliko);
					$( "#update-hours" ).val(hour_neosxoliko);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_neosxoliko);

				}

				else{
				
					$( "#update-days" ).val(day_neosxoliko);
					$( "#update-hours" ).val(hour_neosxoliko);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_neosxoliko);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Ηλέκτρα)		//
			//																		//
			//======================================================================//

			function onClick_ilektra(element_ilektra,aithousa_ilektra){
			
				var day_ilektra = element_ilektra.name;
				var hour_ilektra = element_ilektra.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_ilektra.value == 0){
					for(var h_ilektra = 9; h_ilektra<=20; h_ilektra++){
						$( "#update-days" ).val(day_ilektra);
						$( "#update-hours" ).val(h_ilektra);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_ilektra);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_ilektra.checked == 1){ 
				
					$( "#update-days" ).val(day_ilektra);
					$( "#update-hours" ).val(hour_ilektra);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_ilektra);

				}

				else{
				
					$( "#update-days" ).val(day_ilektra);
					$( "#update-hours" ).val(hour_ilektra);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_ilektra);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Αλκμήνη)		//
			//																		//
			//======================================================================//

			function onClick_alkmini(element_alkmini,aithousa_alkmini){
			
				var day_alkmini = element_alkmini.name;
				var hour_alkmini = element_alkmini.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_alkmini.value == 0){
					for(var h_alkmini = 9; h_alkmini<=20; h_alkmini++){
						$( "#update-days" ).val(day_alkmini);
						$( "#update-hours" ).val(h_alkmini);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_alkmini);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_alkmini.checked == 1){ 
				
					$( "#update-days" ).val(day_alkmini);
					$( "#update-hours" ).val(hour_alkmini);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_alkmini);

				}

				else{
				
					$( "#update-days" ).val(day_alkmini);
					$( "#update-hours" ).val(hour_alkmini);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_alkmini);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			//======================================================================//
		  	//																		//
		  	//		  Αλλαγή βάσης σύμφωνα με τα inputs του χρήστη (Φαίδρα)			//
			//																		//
			//======================================================================//

			function onClick_faidra(element_faidra,aithousa_faidra){
			
				var day_faidra = element_faidra.name;
				var hour_faidra = element_faidra.value;

				//Αν πατήσεις το όνομα της ημέρας άλλαξε όλες τις τιμές της συγκεκριμένης μέρας 
				if(element_faidra.value == 0){
					for(var h_faidra = 9; h_faidra<=20; h_faidra++){
						$( "#update-days" ).val(day_faidra);
						$( "#update-hours" ).val(h_faidra);
						$( "#update-checked" ).val("0");
						$( "#update-aithousa" ).val(aithousa_faidra);

						//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
						$.ajax({
							type:"POST",
							cache: false,
							url: "includes/update-a-diathesimotita.php",
							data: $('#update-status-form').serialize(),
							success: function(data){
								//alert("Η βάση ενημερώθηκε");
							},
							error: function(){
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
								revertFunc();
							},
							async: false,
							traditional: true
						});

					}

				}

				//Αν πατήσεις ένα απλό κουμπί άλλαξε το ανάλογα 
				if(element_faidra.checked == 1){ 
				
					$( "#update-days" ).val(day_faidra);
					$( "#update-hours" ).val(hour_faidra);
					$( "#update-checked" ).val("1");
					$( "#update-aithousa" ).val(aithousa_faidra);

				}

				else{
				
					$( "#update-days" ).val(day_faidra);
					$( "#update-hours" ).val(hour_faidra);
					$( "#update-checked" ).val("0");
					$( "#update-aithousa" ).val(aithousa_faidra);
					
				}
				
				//Επικοινωνία της Javascript με τη PHP για αλλαγή της βάσης 
				$.ajax({
					type:"POST",
					cache: false,
					url: "includes/update-a-diathesimotita.php",
					data: $('#update-status-form').serialize(),
					success: function(data){
						//alert("Η βάση ενημερώθηκε");
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
						revertFunc();
					},
					async: false,
					traditional: true
				});
					
			}

			
			$(function() {

				$( "#tabs" ).tabs();
				$("#tabs").css("display", "block");
				$("#loader").css("display","none");
			});

	</script>

</head>

<body>
	<center>
		<table class="wrapper">
			<tbody>
				<tr>
					<td valign="top">
						<center>
							
							<?php
								require_once("includes/header3.php");
							?>

							<br><br>
							<img id="loader" style="margin-top: 180px; margin-bottom: 227px;" src='img/ajax-loader.gif'></img>
							<div id="tabs" style="border: none; display:none; width: 952px;">
								<ul>
								    <li><a href="#sofoulis" 	style="width:51; font-size:11px">Σοφούλης</a></li>
								    <li><a href="#no1" 			style="width:51; font-size:11px">Νο.1</a></li>
								    <li><a href="#no3" 			style="width:51; font-size:11px">Νο.3</a></li>
								    <li><a href="#dimitra" 		style="width:51; font-size:11px">Δήμητρα</a></li>
								    <li><a href="#lito" 		style="width:51; font-size:11px">Λητώ</a></li>
								    <li><a href="#xaritini" 	style="width:51; font-size:11px">Χαριτίνη</a></li>
								    <li><a href="#mirto" 		style="width:51; font-size:11px">Μυρτώ</a></li>
								    <li><a href="#dorisa" 		style="width:51; font-size:11px">Δόρυσσα</a></li>
								    <li><a href="#neosxoliko" 	style="width:51; font-size:11px">Ν.Σχολικό</a></li>
								    <li><a href="#ilektra" 		style="width:51; font-size:11px">Ηλέκτρα</a></li>
								    <li><a href="#alkmini" 		style="width:51; font-size:11px">Αλκμήνη</a></li>
								    <li><a href="#faidra" 		style="width:51; font-size:11px">Φαίδρα</a></li>
							  	</ul>
							
							<div id="sofoulis">
								<?php
									include("aithouses/sofoulis.php");
								?>
							</div>
							<div id="no1">
								<?php
									include("aithouses/no1.php");
								?>
								
							</div>
							<div id="no3">
								<?php
									include("aithouses/no3.php");
								?>
							    
							</div>
							<div id="dimitra">
								<?php
									include("aithouses/dimitra.php");
								?>
							    
							</div>
							<div id="lito">
								<?php
									include("aithouses/lito.php");
								?>
							    
							</div>
							<div id="xaritini">
								<?php
									include("aithouses/xaritini.php");
								?>
							    
							</div>
							<div id="mirto">
								<?php
									include("aithouses/mirto.php");
								?>
							   
							</div>
							<div id="dorisa">
								<?php
									include("aithouses/dorisa.php");
								?>
							   
							</div>
							<div id="neosxoliko">
								<?php
									include("aithouses/neosxoliko.php");
								?>
							    
							</div>
							<div id="ilektra">
								<?php
									include("aithouses/ilektra.php");
								?>
							    
							</div>
							<div id="alkmini">
								<?php
									include("aithouses/alkmini.php");
								?>
							    
							</div>
							<div id="faidra">
								<?php
									include("aithouses/faidra.php");
								?>
							    
							</div>
							</div>

							<!-- Κρυφή φόρμα για την αλλαγή των τιμών -->
							<div id="update-status-div" style= "display: none;"> 
								<form  id ="update-status-form" name="update-status-form">
									<input type="text" name="update-days" id="update-days"  />
									<input type="text" name="update-hours" id="update-hours"  />
									<input type="text" name="update-checked" id="update-checked"  />
									<input type="text" name="update-aithousa" id="update-aithousa"  />
								</form>
							</div>
							<!--
							//======================================================================//
							//																		//
							//					  Overlay για το loading effect						//
							//																		//
							//======================================================================//
							-->
							
							<div class="modal"></div>
						</center>
					</td>
				</tr>
			</tbody>
		</table>
	</center>
</body>

</html>
<script> 
		
</script>