﻿<?php 
    include("../database.php");

	$sql = "SELECT event_id, parent_id, title, start_date, start_time, end_time FROM Event";
    $result = mysql_query($sql);			
    $events = array();

		while ( $row = mysql_fetch_assoc($result) ){
			$start = $row['start_date'].' '.$row['start_time'];
			$end = $row['start_date'].' '.$row['end_time'];
			$parentID= $row['parent_id'];
			
			$sql_2 = "select ID_Anathesis, repeats, repeat_freq from Event_Parent where parent_id = $parentID";
			$result_2 = mysql_query($sql_2);	
			$row_2 = mysql_fetch_assoc($result_2);
			$ID_Anathesis = $row_2['ID_Anathesis'];

			$sql_3 = "select ID_Parent, ID_TMathimatos from Anathesi where ID_Anathesis = $ID_Anathesis";
			$result_3 = mysql_query($sql_3);
			$row_3 = mysql_fetch_assoc($result_3);
			$ID_Anathesi_Parent = $row_3['ID_Parent'];
			$typos = $row_3['ID_TMathimatos'];
			

			$sql_4 = "select ID_OMathimatos from Anathesi_Parent where ID_Anathesi_Parent = $ID_Anathesi_Parent";
			$result_4 = mysql_query($sql_4);
			$row_4 = mysql_fetch_assoc($result_4);
			$ID_OMathimatos = $row_4['ID_OMathimatos'];

			$sql_5 = "select ID_Eksaminou from Mathima_Orismos where ID_OMathimatos = '$ID_OMathimatos'";
			$result_5 = mysql_query($sql_5);
			$row_5 = mysql_fetch_assoc($result_5);
			$ID_Eksaminou = $row_5['ID_Eksaminou'];
			
			if($ID_Eksaminou == 1 || $ID_Eksaminou == 2){
				$eventArray['id'] = $row['event_id'];
				$eventArray['parent_id'] = $row['parent_id'];
				
				$eventArray['start'] = $start;
				$eventArray['end'] = $end;
				
				$eventArray['title'] = stripslashes($row['title']);
				$eventArray['repeats'] = $row_2['repeats'];
				$eventArray['repeat_freq'] = $row_2['repeat_freq'];
				
				//coloring the event
				if($typos == 0){
					$eventArray['color'] = ""; //"rgb(84, 132, 237)"; //default blue 
				}
				else if($typos == 1){
					$eventArray['color'] = "rgb(81, 183, 73)"; //cool green
				}
				else if($typos == 2){
					$eventArray['color'] = "rgb(220, 33, 39)"; //cool red
				}

				//$eventArray['textColor'] = "rgb(500, 500, 500)"; //text coloring

				$events[] = $eventArray;
			}
		}

    echo json_encode($events);
?>