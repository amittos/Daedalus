﻿<?php
	session_start();
	/*
	if($_SESSION['user_role'] != "2" && $_SESSION['user_role'] != "4"){
		header( 'Location: not_authorized.php' ) ;
	}
	*/
?>
<?php
	include ("database.php");
	
	$ID_Didaskonta = $_GET['id'];
	
	$sql = "SELECT onoma, eponimo FROM Didaskon_Orismos WHERE ID_Didaskonta = $ID_Didaskonta";
	$result = mysql_query($sql);
	$row = mysql_fetch_assoc($result);
	$onoma = $row['onoma'];
	$eponimo = $row['eponimo'];
	$full_name = $onoma." ".$eponimo;
	
	
	
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" lang="el" xml:lang="en"> 
<head>
	
	<!-- JQuery Libray -->
	<script class="jsbin" src="http://code.jquery.com/jquery-1.11.0.js"></script>
	<script src='jquery-ui.custom.min.js'></script>
	<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
	<script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="jquery.dataTables.css">  <!-- Local Installation -->
	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>


	<title> Daedalus </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Language" content="el">	
	<link rel="stylesheet" href="styles/basic/input.css" type="text/css" media="screen"/>
	<link rel="shortcut icon" href="styles/basic/img/favicon.ico" />

	<link rel="stylesheet" href="styles/basic/style.css">

	<script>

		$(document).ready( function () { //Script t?? DataTable
		    $('#table_id').DataTable();
		} );
		
		$(function() {	//Κώδικας για εισαγωγή ωρας
			$( "input[type=submit]" )
				.button()
				.click(function( event ) {
				event.preventDefault();
				$.ajax({
					type:"POST",
					url: "includes/insert-tutor-diathesimotita.php",
					data: $('#insert-diathesimotita-form').serialize(),
					success: function(){
						
						location.reload(); // =refresh page
						
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την διαγραφή. Παρακαλώ προσπαθήστε ξανά.");
					}
				});
			});
		 }); 
		 
		 function onDeleteClick(ID_DD){ 	//OnClick του delete button
		
			$("#delete_diathesimotita_id" ).val(ID_DD);
			
			$.ajax({
					type:"POST",
					url: "includes/delete-diathesimotita.php",
					data: $('#delete_diathesimotita_form').serialize(),
					success: function(){
						
						location.reload();
						
					},
					error: function(){
						alert("Υπήρξε ένα πρόβλημα κατά την διαγραφή. Παρακαλώ προσπαθήστε ξανά.");
					}
				});
		
		}
		
	</script>
	<body link="#5C5C5C" vlink="#7A7A7A" alink="#A3A3A3"  width="1200px"> <!-- Άλλαξε το χρώμα των links -->
	<center>
		<table class="wrapper">
			<tbody>
				<tr>
					<td valign="top">
						<center  width="1200px">
							
							<?php
								require_once("includes/header3.php");
							?>

							<div id="insert-tutor-div" align="left" style="margin-left:10px; margin-bottom:25px; margin-top:34px;" title="Anathesi Mathimatos">
								<form action="includes/insert-tutor-diathesimotita.php" method="post" id ="insert-diathesimotita-form" name="insert-diathesimotita-form">
									<table>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label >Όνομα:</label></td>
													<td><label ><?php echo $full_name; ?></label></td>
													<input type="hidden" value="<?php echo $ID_Didaskonta; ?>" name="tutor_id" id ="tutor_id"/>
												</p>
											</div>
										</tr>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="imera">Ημέρα:</label></td>
													<td>
														<select name="imera" id="imera">
														  <option value="1">Δευτέρα</option>
														  <option value="2">Τρίτη</option>
														  <option value="3">Τετάρτη</option>
														  <option value="4">Πέμπτη</option>
														  <option value="5">Παρασκευή</option>
														  <option value="6">Σάββατο</option>
														  <option value="0">Κυριακή</option>
														</select>
													</td>
												</p>
											</div>
										</tr>
										<tr>
										<p>
											<td><label for="apo">Από:</label></td>
											<td>
												<select name="apo" id="apo">
												  <option value="9">09:00</option>
												  <option value="10">10:00</option>
												  <option value="11">11:00</option>
												  <option value="12">12:00</option>
												  <option value="13">13:00</option>
												  <option value="14">14:00</option>
												  <option value="15">15:00</option>
												  <option value="16">16:00</option>
												  <option value="17">17:00</option>
												  <option value="18">18:00</option>
												  <option value="19">19:00</option>
												  <option value="20">20:00</option>
												</select>
											</td>
										</p>
										</tr>
										<tr>
										<p>
											<td><label for="eos">Έως και:</label></td>
											<td>
												<select name="eos" id="eos">
												  <option value="9">09:00</option>
												  <option value="10">10:00</option>
												  <option value="11">11:00</option>
												  <option value="12">12:00</option>
												  <option value="13">13:00</option>
												  <option value="14">14:00</option>
												  <option value="15">15:00</option>
												  <option value="16">16:00</option>
												  <option value="17">17:00</option>
												  <option value="18">18:00</option>
												  <option value="19">19:00</option>
												  <option value="20">20:00</option>
												</select>
											</td>
										</p>
										</tr>
										<tr  id="submit">
											<td><input type="submit" value="Εισαγωγή"> </td>
										</tr>
									</table>
								</form>
							</div>
							<table id="table_id" class="display">
								<thead>
									<tr>
							            <th align="left">Μέρα</th>
							            <th align="left">Από</th>
							            <th align="left">Έως</th>
							            <th align="left"></th>
							        </tr>
								</thead>
								<tbody>

								<?php
									
									
									
									$sql2 = "select ID_DD, start, end from Didaskon_Arxiki_Diathesimotita WHERE ID_Didaskonta = '$ID_Didaskonta' "; // Διάβασε Όνομα και ID μαθήματος από την βάση
									$result2 = mysql_query($sql2); // Βάλ'τα στο result
									$num_rows = mysql_num_rows($result2);
									
									while ($row2 = mysql_fetch_assoc($result2)){
										
										$ID_DD = $row2['ID_DD'];
										$start = $row2['start'];
										$end = $row2['end'];
										
										$sql3 = "SELECT ID_Hour, ID_Day FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$start' ";
										$result3 = mysql_query($sql3);
										$row3 = mysql_fetch_assoc($result3);
										$start_hour_id = $row3['ID_Hour'];
										$start_day_id = $row3['ID_Day'];
										
										$sql4 = "SELECT ID_Hour FROM Hour_Per_Day WHERE ID_Hour_Per_Day = '$end' ";
										$result4 = mysql_query($sql4);
										$row4 = mysql_fetch_assoc($result4);
										$end_hour_id = $row4['ID_Hour'];
										
										$sql5 = "SELECT perigrafi FROM Hour WHERE ID_Hour = $start_hour_id";
										$result5 = mysql_query($sql5);
										$row5 = mysql_fetch_assoc($result5);
										$start_hour = $row5['perigrafi'];
										
										$sql6 = "SELECT perigrafi FROM Hour WHERE ID_Hour = $end_hour_id";
										$result6 = mysql_query($sql6);
										$row6 = mysql_fetch_assoc($result6);
										$end_hour = $row6['perigrafi'];
										
										$sql7 = "SELECT perigrafi FROM Day WHERE ID_Day = $start_day_id";
										$result7 = mysql_query($sql7);
										$row7 = mysql_fetch_assoc($result7);
										$start_day = $row7['perigrafi'];
										
									?>
										<tr>
											<td><?php echo $start_day; ?></td> <!-- Εκτύπωσε τα αποτελέσματα -->
											<td><?php echo $start_hour; ?></td>
											<td><?php echo $end_hour; ?></td>
											<td align="center"><div style="padding-top: 4px; padding-bottom: 4px; width: 17px;" onmouseover="$(this).find('img').attr('src','styles/basic/img/delete2.png');"  
										onmouseout="$(this).find('img').attr('src','styles/basic/img/delete.png');" onclick="onDeleteClick(<?php echo $ID_DD ?>)"> <img src="styles/basic/img/delete.png" style="height:10px; width:10px;" /></div></td>
										</tr>
								<?php
								
									}
									
								?>
								<div id="delete_diathesimotita_div" style= "display: none;"> 
									<form  id ="delete_diathesimotita_form" name="delete_diathesimotita_form">
										<input type="text" name="delete_diathesimotita_id" id="delete_diathesimotita_id"  />
									</form>
								</div>
									
								</tbody>
							</table>
						</center>
					<td>
				</tr>
			</tbody>
		</table>
	</center>
	</body>
</html>	
