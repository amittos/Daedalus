<?php
	session_start();
	include("database.php");

	ini_set('display_errors',1);  
	error_reporting(E_ERROR);
	
	$toCheck = false;
	// ======================================================================//
	// 																		 //
	//			 			Διάβασμα ενεργού ημερολογίου					 //
	// 																		 //
	// ======================================================================//

	$sql_active_calendar = "SELECT start, end FROM Energo_Imerologio WHERE ID_Energo_Imerologio = 1";
	$row_active_calendar = mysql_fetch_assoc(mysql_query($sql_active_calendar));
	$active_calendar_start_temp = $row_active_calendar['start'];

	$active_calendar_start = date("d-M-Y", strtotime($active_calendar_start_temp));
	$active_calendar_end = date("d-M-Y", strtotime($active_calendar_end_temp));

	// ======================================================================//
	// 																		//
	// 			Διάβασμα Μαθημάτων για το autocomplete (Συνολικό)			//
	// 																		//
	// ======================================================================//

	$string = "lol";

	$data = array();  //Φτιάξε έναν πίνακα με όνομα data
	$data_ena = array();
	$data_dio = array(); 
	$data_tria = array();
	$data_tessera = array();
	$data_pente = array();

	$sql = "SELECT ID_Anathesis, ID_Parent, label FROM Anathesi";
	$result = mysql_query($sql);  // Βάλ'τα στο result
	
	while($row = mysql_fetch_assoc($result)){

		$ID_Anathesis = $row['ID_Anathesis'];
		$ID_Anathesi_Parent = $row['ID_Parent'];
		$label = $row['label'];

		$sql_parent = "SELECT ID_OMathimatos FROM Anathesi_Parent WHERE ID_Anathesi_Parent = $ID_Anathesi_Parent";
		$result_parent = mysql_query($sql_parent);
		$row_parent = mysql_fetch_assoc($result_parent);
		$ID_OMathimatos = $row_parent['ID_OMathimatos'];

	  	$sql_mathima_orismos = "SELECT onoma AS label, ID_Eksaminou AS eksamino FROM Mathima_Orismos WHERE ID_OMathimatos = '$ID_OMathimatos'";
	  	$result_mathima_orismos = mysql_query($sql_mathima_orismos);

		$row_mathima_orismos = mysql_fetch_object($result_mathima_orismos);
		$onoma_mathimatos = $row_mathima_orismos->label;
		$eksamino = $row_mathima_orismos->eksamino;
		

		$info = new stdClass();
		$info = (object) array('value' => $ID_Anathesis, 'label' => $label);
		$data[] = $info;

		if ( $eksamino == 1 OR $eksamino == 2) // Καταχώρηση μόνο αν ανήκει στο σωστό εξάμηνο
		{
			$data_ena[] = $info;
			//echo "1o etos ";
		}
		else if ( $eksamino == 3 OR $eksamino == 4) 
		{
			$data_dio[] = $info;
			//echo "2o etos ";
		}
		else if ( $eksamino == 5 OR $eksamino == 6) 
		{
			$data_tria[] = $info;
			//echo "3o etos ";
		}
		else if ( $eksamino == 8 OR $eksamino == 7) // Αυτά τα έβαλα αντίστροφα για το σπάσιμο  
		{
			$data_tessera[] = $info;
		}
		else if ( $eksamino == 9 OR $eksamino == 10)  
		{
			$data_pente[] = $info;
		}
	}
	
	$sql_aithousa = "SELECT ID_Aithousas AS value, onoma AS label FROM Aithousa_Orismos"; // Διάβασε Όνομα και ID μαθήματος από την βάση
	$result_aithousa = mysql_query($sql_aithousa); // Βάλ'τα στο result
	$data_aithousa = array(); // Φτιάξε έναν πίνακα με όνομα data

	while ( $row_aithousa = mysql_fetch_object($result_aithousa) ) // Βάλε όλα τα στοιχεία στον πίνακα data
	{
	  $data_aithousa[] = $row_aithousa;
	}
		
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" lang="el" xml:lang="en"> 

<head>

	<!--
	//======================================================================//
	//																		//
	//								Libraries								//
	//																		//
	//======================================================================//
	-->

	<title> Ημερολόγιο - Δαίδαλος </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Language" content="el">

	<link rel="stylesheet" href="styles/basic/style.css"> <!-- New CSS of Doom for the Header -->
	<link rel="shortcut icon" href="https://pithos.grnet.gr/pithos/rest/icsd08158@aegean.gr/files/favicon.icon" />
	
	<link rel="stylesheet" href="styles/basic/jquery-ui.css"> <!-- JQuery βιβλιοθήκη, με πειραγμένα τα Tabs -->

	<!-- FullCalendar stylesheet and FullCalendar JS file -->
	<link rel='stylesheet' type='text/css' href='fullcalendar.css' />
	<script type='text/javascript' src='fullcalendar.js'></script>
	<script src='lib/moment.js'></script>
	<script src='lib/jquery-ui.custom-datepicker.js'></script>
	<script src='lang-all.js'></script>
	<script src='lang/el.js'></script>
	
	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="jquery.dataTables.css">  <!-- Local Installation -->
	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>

	<style>
		#copy-main .ui-widget-header{
		    background:white !important;
		}

		#copy-main .ui-tabs{
		    padding: 0 !important;
		    margin-top:40px !important;
		    position:relative !important;
		}

		#copy-main .ui-tabs .ui-tabs-nav{
		    padding: 0 !important;
		    position:absolute !important;
		    margin:-32px -1px 0 !important; //compensation
		}
		.ui-autocomplete-loading {
		   background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;
		}
		
	  	#project-description {
		    margin: 0;
		    padding: 0;
		}
		#wrapper{
			/*background: #6AA4C1;*/
		}
		.modal {
		    display:    none;
		    position:   fixed;
		    z-index:    1000;
		    top:        0;
		    left:       0;
		    height:     100%;
		    width:      100%;
		    background: rgba( 255, 255, 255, .8 ) 
		                url('img/ajax-loader.gif') 
		                50% 50% 
		                no-repeat;
		}

		/* When the body has the loading class, we turn
		   the scrollbar off with overflow:hidden */
		body.loading {
		    overflow: hidden;   
		}

		/* Anytime the body has the loading class, our
		   modal element will be visible */
		body.loading .modal {
		    display: block;
		}
		p{
			line-height: 16px;
			margin: 0 0 0 0;
		}
		::-webkit-scrollbar {
	       width: 4px;
		} 
		::-webkit-scrollbar-track {
            background-color: rgb(241, 241, 241);
            border-radius: 6px;
		}
		::-webkit-scrollbar-thumb {
        	background-color: rgba(0, 0, 0, 0.2); 
        	border-radius: 6px;
		}
		.fc-agenda-slots td div {
	         height: 40px !important;
	    }
	</style>

  	<!--
	//======================================================================//
	//																		//
	//				Εισαγωγή Full Calendar κάθε εξαμήνου					//
	//																		//
	//======================================================================//
	-->

	<?php
		
  		include("eti/etos_synoliko.php");
  		include("eti/etos_ena.php");
  		include("eti/etos_dio.php");
  		include("eti/etos_tria.php");
  		include("eti/etos_tessera.php");
  		include("eti/etos_pente.php");
  		
  	?>

  	<!--
	//======================================================================//
	//																		//
	//			   Script για την ενεργοποίηση της JQuery Tabs				//
	//																		//
	//======================================================================//
	-->

  	<script>

		$(function() {
			
			$( "#accordion_ena" ).accordion({
				collapsible: true,
				heightStyle: "content",
				header: "> div > h3",
				active: false
			}).sortable({
		        axis: "y",
		        handle: "h3",
		        stop: function( event, ui ) {
		            // IE doesn't register the blur when sorting
	                // so trigger focusout handlers to remove .ui-state-focus
		            ui.item.children( "h3" ).triggerHandler( "focusout" );
		 
		        	// Refresh accordion to handle new order
		        	$( this ).accordion( "refresh" );
		        }
		    });

		    $( "#accordion_dio" ).accordion({
				collapsible: true,
				heightStyle: "content",
				header: "> div > h3",
				active: false
			}).sortable({
		        axis: "y",
		        handle: "h3",
		        	// Refresh accordion to handle new order
		        	$( this ).accordion( "refresh" );
		        }
		    });
		    
		    $( "#accordion_tria" ).accordion({
				collapsible: true,
				heightStyle: "content",
				header: "> div > h3",
				active: false
			}).sortable({
		        axis: "y",
		        handle: "h3",
		        stop: function( event, ui ) {
		            // IE doesn't register the blur when sorting
	                // so trigger focusout handlers to remove .ui-state-focus
		            ui.item.children( "h3" ).triggerHandler( "focusout" );
		 
		        	// Refresh accordion to handle new order
		        	$( this ).accordion( "refresh" );
		        }
		    });
		    
		    $( "#accordion_tessera" ).accordion({
				collapsible: true,
				heightStyle: "content",
				header: "> div > h3",
				active: false
			}).sortable({
		        axis: "y",
		        handle: "h3",
		        stop: function( event, ui ) {
		            // IE doesn't register the blur when sorting
	                // so trigger focusout handlers to remove .ui-state-focus
		            ui.item.children( "h3" ).triggerHandler( "focusout" );
		 
		        	// Refresh accordion to handle new order
		        	$( this ).accordion( "refresh" );
		        }
		    });
		    
		    $( "#accordion_pente" ).accordion({
				collapsible: true,
				heightStyle: "content",
				header: "> div > h3",
				active: false
			}).sortable({
		        axis: "y",
		        handle: "h3",
		        stop: function( event, ui ) {
		            // IE doesn't register the blur when sorting
	                // so trigger focusout handlers to remove .ui-state-focus
		            ui.item.children( "h3" ).triggerHandler( "focusout" );
		 
		        	// Refresh accordion to handle new order
		        	$( this ).accordion( "refresh" );
		        }
		    });
		    
		    $( "#accordion_synoliko" ).accordion({
				collapsible: true,
				heightStyle: "content",
				header: "> div > h3",
				active: false
			}).sortable({
		        axis: "y",
		        handle: "h3",
		        stop: function( event, ui ) {
		            // IE doesn't register the blur when sorting
	                // so trigger focusout handlers to remove .ui-state-focus
		            ui.item.children( "h3" ).triggerHandler( "focusout" );
		 
		        	// Refresh accordion to handle new order
		        	$( this ).accordion( "refresh" );
		        }
		    });

			$('#tabs').tabs({
				//Κάνε render to Full Calendar κάθε έτους ώστε να λειτουργεί μέσα στα tabs της JQuery
			    activate: function(event, ui) {
			        $('#calendar').fullCalendar('render'); 
			        $('#calendar_ena').fullCalendar('render');
			        $('#calendar_dio').fullCalendar('render');
			        $('#calendar_tria').fullCalendar('render');
			        $('#calendar_tessera').fullCalendar('render');
			        $('#calendar_pente').fullCalendar('render');

					$('#calendar_dio').fullCalendar('refetchEvents');
					$('#calendar_tria').fullCalendar('refetchEvents');
				    $('#calendar_pente').fullCalendar('refetchEvents');
				   	$('#calendar_tessera').fullCalendar('refetchEvents');


					var activeTab = $('#tabs').tabs('option', 'active');
					
					if (activeTab == 0) {
						mathimataTags = <?php echo json_encode($data); ?>
					}
					if (activeTab == 1) {
						mathimataTags = <?php echo json_encode($data_dio); ?>
					}
					if (activeTab == 2) {
						mathimataTags = <?php echo json_encode($data_tria); ?> 
					}
					if (activeTab == 3) {
						mathimataTags = <?php echo json_encode($data_tessera); ?>
					}
					if (activeTab == 4) {
						mathimataTags = <?php echo json_encode($data_pente); ?>
					}
					if (activeTab == 5) {
						mathimataTags = <?php echo json_encode( $data ); ?>
					}


					
			    }
			});
		});

		
		$(document).ready( function () { //Script του DataTable
		    $('#constraints_ena').DataTable();
		    $('#constraints_dio').DataTable();
		    $('#constraints_tria').DataTable();
		    $('#constraints_tessera').DataTable();
		    $('#constraints_pente').DataTable();
		    $('#constraints').DataTable();

		} );
		
		$('#tabs').click('tabsselect', function (event, ui) {
			 var selectedTab = $("#tabs").tabs('option','active');
			 alert(mathimataTags);
		});
		
		$(function() {
		   	var tooltips = $( "#tabs" ).tooltip({
		     	position: {
		     	}
		    });
	    });



		function changeActiveCalendar() {

			$('#change_active_calendar')
			.dialog({
				height: 'auto',
				width: '317px',
				autoResize:true,
				modal: true,
				resizable: false,
				
				open: function(){
					$( "#change_active_calendar_start" ).datepicker({
		       		 	showOtherMonths: true,
				        selectOtherMonths: true,
					 	numberOfMonths: 2,
      					showButtonPanel: true,
      					dateFormat: "yy-mm-dd"
				    }).blur();
			    	$( "#change_active_calendar_end" ).datepicker({
			 	        showOtherMonths: true,
				        selectOtherMonths: true,
					 	numberOfMonths: 2,
      					showButtonPanel: true,
      					dateFormat: "yy-mm-dd"
				    });
				},
				buttons: {
					"Save": function() { // Υλοποίηση του button save
						
						$.ajax({
							type:"POST",
							url: "includes/update_active_calendar.php",
							data: $('#change_active_calendar_form').serialize(),
							success: function(response){
								
								//alert(response);
								//Μηδενισμός όλων των πεδίων
								$('#change_active_calendar_start').val("");
								$('#change_active_calendar_end').val("");
								location.reload();
								
							},
							error: function(response){
								alert(response);
								alert("Υπήρξε ένα πρόβλημα κατά την εισαγωγή. Παρακαλώ προσπαθήστε ξανά.");
							}
						});
						$(this).dialog("close");
					},

					Cancel: function() { // Υλοποίηση του button cancel
						$(this).dialog("close");
					}
				}
			
			});
			$("#ui-datepicker-div").css("z-index", "9999"); 
		}

	</script>

	<!--
	//======================================================================//
	//																		//
	//							Κώδικας HTML								//
	//																		//
	//======================================================================//
	-->

	<link rel="stylesheet" href="styles/basic/input.css" type="text/css" media="screen"/>
	<link rel="shortcut icon" href="styles/basic/img/favicon.ico" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'> <!-- Google Fonts -->
	
	<style>
		.ui-autocomplete {
			position: absolute;
			top: 0;
			left: 0;
			cursor: default;
			z-index: 1500; <!-- Φέρε το autocomplete μπροστά, (override του JQuery UI) -->
		}
		.ui-tooltip {
   			 padding-bottom: 0px;
   			 padding-top: 0px;
   			 max-width: 200px;
   			 font-size: 11px;
   			 border: 10px;
   			 line-height: 15px;

  		}
  		.ui-datepicker { width: 17em; padding: .2em .2em 0; z-index:9999; }
	</style>
	
</head>
<body>
	<center>
		<table id="wrapper" style="height: 100%;">
			<tbody>
				<tr>
					<td valign="top">
						<center>
							
							<?php
								require_once("includes/header3.php");
							?>
<!--
							//======================================================================//
							//																		//
							//						Φόρμα Change Active Calendar					//
							//																		//
							//======================================================================//
							-->
							
							<div id="change_active_calendar" title="Αλλαγή ενεργού ημερολογίου" style= "display: none;" width="1200px">
								<form action="" id ="change_active_calendar_form" name="change_active_calendar_form">
									<table>
										<fieldset>
											<tr style="margin-top:35px; margin-bottom:35px;">
												<div class="ui-widget">
													<p>
														<td><label for="change_active_calendar_start">Ημ/νια Έναρξης Εξαμήνου:</label></td>
														<td style="padding-left: 4px;"><input type="text" value=<?php echo $active_calendar_start_temp;?> name="change_active_calendar_start" id="change_active_calendar_start"/></td>
													</p>
												</div>
											</tr style="margin-top:35px; margin-bottom:35px;">
											<tr>
												<div class="ui-widget">
													<p>
														<td><label for="change_active_calendar_end">Ημ/νια Λήξης Εξαμήνου:</label></td>
														<td style="padding-left: 4px;"><input type="text" value=<?php echo $active_calendar_end_temp;?> name="change_active_calendar_end" id="change_active_calendar_end"   /></td>
													</p>
												</div>
											</tr>
										</fieldset>
									</table>
								</form>
							</div>

							<!--
							//======================================================================//
							//																		//
							//							Φόρμα Add Event	Συνολικο					//
							//																		//
							//======================================================================//
							-->

							<div id="add-event" title="Εισαγωγή Καταχώρησης" style= "display: none;" width="1200px">
								<form action="" id ="add-event-form" name="add-event-form">
									<table>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="title">Μάθημα</label></td>
													<td><input type="text" name="title" id="title" data-bv-notempty data-bv-notempty-message="The gender is required"  /></td>
													<input type="hidden" name="ID-Anathesis" id="ID-Anathesis" readonly />
													<input type="hidden" name="eksamino" id="eksamino" readonly />
												</p>
											</div>
										</tr>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="aithousa">Αίθουσα</label></td>
													<td><input type="text" name="aithousa" id="aithousa"  required  /></td>
													<input type="hidden" name="ID-Aithousas" id="ID-Aithousas" readonly />
												</p>
											</div>
										</tr>
										<tr>
										<p>
											<td><label for="event-date">Ημερομηνία</label></td>
											<td><input type="text" name="event-date" id="event-date" tabindex="-1" readonly /></td>
										</p>
										</tr>
										<tr>
											<td><label for="start-time">Έναρξη</label></td>
											<td><input type="text" name="start-time" id="start-time" readonly /></td>
										</tr>
										<tr>
											<td><label for="end-time">Λήξη</label></td>
											<td><input type="text" name="end-time" id="end-time" readonly /></td>
										</tr>
										<tr  id="repeat-options">
											<td>Repeat every: </td>
											 <td>
												<div id="add-repeat"> 
													<input type="radio" value="7" name="repeat-freq" id="repeat-freq-7" align="bottom" checked="checked"><label for="repeat-freq-7">Weekly</label>
													<input type="radio" value="14" name="repeat-freq" id="repeat-freq-14" align="bottom"><label for="repeat-freq-14">2 weels</label>
												</div>
										</tr>
										<tr id="typosRadio"></tr >
										<tr id="tmimaMenu"></tr >
									</table>
								</form>
							</div>

							<!--
							//======================================================================//
							//																		//
							//							Φόρμα Add Event για το 1ο ετος				//
							//																		//
							//======================================================================//
							-->

							<div id="add-event-ena" title="Εισαγωγή Καταχώρησης" style= "display: none;" width="1200px">
								<form action="" id ="add-event-form-ena" name="add-event-form-ena">
									<table id="add-event-table-ena">
										<tr>
											<div>
												<p>
													<td><label for="title-ena">Μάθημα</label></td>
													<td><input title="Πληκτρολογήστε το πρώτο γράμμα του μαθήματος"  type="text" name="title-ena" id="title-ena"  /></td>
													<input type="hidden" name="ID-Anathesis-ena" id="ID-Anathesis-ena" readonly />
													<input type="hidden" name="eksamino-ena" id="eksamino-ena" readonly /><p id="project-description"></p>
												</p>
											</div>
										</tr>
										<tr>
											<div>
												<p>
													<td><label for="aithousa-ena">Αίθουσα</label></td>
													<td><input title="Πληκτρολογήστε το πρώτο γράμμα της αίθουσας" type="text" name="aithousa-ena" id="aithousa-ena"  /></td>
													<input type="hidden" name="ID-Aithousas-ena" id="ID-Aithousas-ena" readonly />
												</p>
											</div>
										</tr>
										<tr>
										<p>
											<td><label for="event-date-ena">Ημερομηνία</label></td>
											<td><input type="text" name="event-date-ena" id="event-date-ena" tabindex="-1" readonly /></td>
										</p>
										</tr>
										<tr>
											<td><label for="start-time-ena">Έναρξη</label></td>
											<td><input type="text" name="start-time-ena" id="start-time-ena" readonly /></td>
										</tr>
										<tr>
											<td><label for="end-time-ena">Λήξη</label></td>
											<td><input type="text" name="end-time-ena" id="end-time-ena" readonly /></td>
											<input type="hidden" value="7" name="repeat-freq" id="repeat-freq-7">
										</tr>
										</br>
										<tr id="typosRadio-ena"></tr >
										<tr id="tmimaMenu-ena"></tr >
									</table>
								</form>
							</div>

							<!--
							//======================================================================//
							//																		//
							//							Φόρμα Add Event για το 2ο ετος				//
							//																		//
							//======================================================================//
							-->

							<div id="add-event-dio" title="Εισαγωγή Καταχώρησης" style= "display: none;" width="1200px">
								<form action="" id ="add-event-form-dio" name="add-event-form-dio">
									<table id="add-event-table-dio">
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="title-dio">Μάθημα</label></td>
													<td><input type="text" name="title-dio" id="title-dio"  /></td>
													<input type="hidden" name="ID-Anathesis-dio" id="ID-Anathesis-dio" readonly />
													<input type="hidden" name="eksamino-dio" id="eksamino-dio" readonly />

												</p>
											</div>
										</tr>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="aithousa-dio">Αίθουσα</label></td>
													<td><input type="text" name="aithousa-dio" id="aithousa-dio"  /></td>
													<input type="hidden" name="ID-Aithousas-dio" id="ID-Aithousas-dio" readonly />
												</p>
											</div>
										</tr>
										<tr>
										<p>
											<td><label for="event-date-dio">Ημερομηνία</label></td>
											<td><input type="text" name="event-date-dio" id="event-date-dio" tabindex="-1" readonly /></td>
										</p>
										</tr>
										<tr>
											<td><label for="start-time-dio">Έναρξη</label></td>
											<td><input type="text" name="start-time-dio" id="start-time-dio" readonly /></td>
										</tr>
										<tr>
											<td><label for="end-time-dio">Λήξη</label></td>
											<td><input type="text" name="end-time-dio" id="end-time-dio" readonly /></td>
											<input type="hidden" value="7" name="repeat-freq" id="repeat-freq-7">
										</tr>
										</br>
										</br>
										<tr id="typosRadio-dio"></tr >
										<tr id="tmimaMenu-dio"></tr >
									</table>
								</form>
							</div>

							<!--
							//======================================================================//
							//																		//
							//							Φόρμα Add Event για το 3ο ετος				//
							//																		//
							//======================================================================//
							-->

							<div id="add-event-tria" title="Εισαγωγή Καταχώρησης" style= "display: none;" width="1200px">
								<form action="" id ="add-event-form-tria" name="add-event-form-tria">
									<table id="add-event-table-tria">
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="title-tria">Μάθημα</label></td>
													<td><input type="text" name="title-tria" id="title-tria"  /></td>
													<input type="hidden" name="ID-Anathesis-tria" id="ID-Anathesis-tria" readonly />
													<input type="hidden" name="eksamino-tria" id="eksamino-tria" readonly />

												</p>
											</div>
										</tr>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="aithousa-tria">Αίθουσα</label></td>
													<td><input type="text" name="aithousa-tria" id="aithousa-tria"  /></td>
													<input type="hidden" name="ID-Aithousas-tria" id="ID-Aithousas-tria" readonly />
												</p>
											</div>
										</tr>
										<tr>
										<p>
											<td><label for="event-date-tria">Ημερομηνία</label></td>
											<td><input type="text" name="event-date-tria" id="event-date-tria" tabindex="-1" readonly /></td>
										</p>
										</tr>
										<tr>
											<td><label for="start-time-tria">Έναρξη</label></td>
											<td><input type="text" name="start-time-tria" id="start-time-tria" readonly /></td>
										</tr>
										<tr>
											<td><label for="end-time-tria">Λήξη</label></td>
											<td><input type="text" name="end-time-tria" id="end-time-tria" readonly /></td>
											<input type="hidden" value="7" name="repeat-freq" id="repeat-freq-7">
										</tr>
										</br>
										</br>
										<tr id="typosRadio-tria"></tr >
										<tr id="tmimaMenu-tria"></tr >
									</table>
								</form>
							</div>

							<!--
							//======================================================================//
							//																		//
							//							Φόρμα Add Event για το 4ο ετος				//
							//																		//
							//======================================================================//
							-->

							<div id="add-event-tessera" title="Εισαγωγή Καταχώρησης" style= "display: none;" width="1200px">
								<form action="" id ="add-event-form-tessera" name="add-event-form-tessera">
									<table id="add-event-table-tessera">
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="title-tessera">Μάθημα</label></td>
													<td><input type="text" name="title-tessera" id="title-tessera"  /></td>
													<input type="hidden" name="ID-Anathesis-tessera" id="ID-Anathesis-tessera" readonly />
													<input type="hidden" name="eksamino-tessera" id="eksamino-tessera" readonly />

												</p>
											</div>
										</tr>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="aithousa-tessera">Αίθουσα</label></td>
													<td><input type="text" name="aithousa-tessera" id="aithousa-tessera"  /></td>
													<input type="hidden" name="ID-Aithousas-tessera" id="ID-Aithousas-tessera" readonly />
												</p>
											</div>
										</tr>
										<tr>
										<p>
											<td><label for="event-date-tessera">Ημερομηνία</label></td>
											<td><input type="text" name="event-date-tessera" id="event-date-tessera" tabindex="-1" readonly /></td>
										</p>
										</tr>
										<tr>
											<td><label for="start-time-tessera">Έναρξη</label></td>
											<td><input type="text" name="start-time-tessera" id="start-time-tessera" readonly /></td>
										</tr>
										<tr>
											<td><label for="end-time-tessera">Λήξη</label></td>
											<td><input type="text" name="end-time-tessera" id="end-time-tessera" readonly /></td>
											<input type="hidden" value="7" name="repeat-freq" id="repeat-freq-7">
										</tr>
										</br>
										</br>
										<tr id="typosRadio-tessera"></tr >
										<tr id="tmimaMenu-tessera"></tr >
									</table>
								</form>
							</div>

							<!--
							//======================================================================//
							//																		//
							//							Φόρμα Add Event για το 5ο ετος				//
							//																		//
							//======================================================================//
							-->

							<div id="add-event-pente" title="Εισαγωγή Καταχώρησης" style= "display: none;" width="1200px">
								<form action="" id ="add-event-form-pente" name="add-event-form-pente">
									<table id="add-event-table-pente">
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="title-pente">Μάθημα</label></td>
													<td><input type="text" name="title-pente" id="title-pente"  /></td>
													<input type="hidden" name="ID-Anathesis-pente" id="ID-Anathesis-pente" readonly />
													<input type="hidden" name="eksamino-pente" id="eksamino-pente" readonly />

												</p>
											</div>
										</tr>
										<tr>
											<div class="ui-widget">
												<p>
													<td><label for="aithousa-pente">Αίθουσα</label></td>
													<td><input type="text" name="aithousa-pente" id="aithousa-pente"  /></td>
													<input type="hidden" name="ID-Aithousas-pente" id="ID-Aithousas-pente" readonly />
												</p>
											</div>
										</tr>
										<tr>
										<p>
											<td><label for="event-date-pente">Ημερομηνία</label></td>
											<td><input type="text" name="event-date-pente" id="event-date-pente" tabindex="-1" readonly /></td>
										</p>
										</tr>
										<tr>
											<td><label for="start-time-pente">Έναρξη</label></td>
											<td><input type="text" name="start-time-pente" id="start-time-pente" readonly /></td>
										</tr>
										<tr>
											<td><label for="end-time-pente">Λήξη</label></td>
											<td><input type="text" name="end-time-pente" id="end-time-pente" readonly /></td>
											<input type="hidden" value="7" name="repeat-freq" id="repeat-freq-7">
										</tr>
										</br>
										</br>
										<tr id="typosRadio-pente"></tr >
										<tr id="tmimaMenu-pente"></tr >
									</table>
								</form>
							</div>


							<!--
							//======================================================================//
							//																		//
							//							Φόρμα Update Event							//
							//																		//
							//======================================================================//
							-->
							
							<div id="update-event" title="Διαγραφή" style= "display: none;">
								<form action="" id ="update-event-form" name="update-event-form">
									<table>
										<tr>
											<div class="ui-widget">
												<td><label for="update-title">Τίτλος</label></td>
												<td><input type="text" style="width:300px;" name="update-title" id="update-title"  readonly/></td>
												<input type="hidden" name="update-calendar-id" id="update-calendar-id"  />
												<input type="hidden" name="update-ID-OMathima" id="update-ID-OMathima" readonly/>
											</div>
										</tr>
										<tr>
											<input type="hidden" name="update-event-date" id="update-event-date" tabindex="-1"/>
											<input type="hidden" name="update-start-time" id="update-start-time" tabindex="-1"/>
											<input type="hidden" name="update-end-time" id="update-end-time" tabindex="-1"/>
											<input type="hidden" name="update-day-delta" id="update-day-delta" tabindex="-1"/>
										</tr>
										<tr>
											<td><label for="update-show-time">Ωράριο</label></td>
											<td><input type="text" style="width:300px;" name="update-show-time" id="update-show-time" readonly/></td>
										</tr>
									</table>
								</form>
							</div>
							<div id="dialog-confirm" title="Διαγραφή μαθήματος" style="display:none;">
								<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Είστε σίγουροι ότι θέλετε να διαγράψετε αυτό το μάθημα;</p>
							</div>
							<div id="delete-dialog-confirm" title="Διαγραφή μαθήματος" style="display:none;">
								<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Είστε σίγουροι ότι θέλετε να διαγράψετε αυτό το μάθημα;</p>
							</div>
							<div id="update-dialog-confirm" title="Τροποποίηση μαθήματος" style="display:none;">
								<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Θα θέλατε να τροποποιήσετε μόνο αυτό το στιγμιότυπό, αυτό και τα μελλοντικά ή όλα όσα υπάρχουν στο ημερολόγιο;</p>
							</div>

							<!--
							//======================================================================//
							//																		//
							//			JQuery Tabs που κάνουν import το κάθε ημερολόγιο			//
							//																		//
							//======================================================================//
							-->

							<div id="tabs" style="border: none;" >
								<ul>
									<li><a href="#etos_ena" 		style="width:172; font-size:11px">1o Έτος</a></li>
									<li><a href="#etos_dio" 	 	style="width:172; font-size:11px">2ο Έτος</a></li>
									<li><a href="#etos_tria" 	 	style="width:172; font-size:11px">3ο Έτος</a></li>
									<li><a href="#etos_tessera"  	style="width:172; font-size:11px">4ο Έτος</a></li>
									<li><a href="#etos_pente" 	 	style="width:172; font-size:11px">5ο Έτος</a></li>
									<li><a href="#etos_synoliko" 	style="width:172; font-size:11px">Συνολικό</a></li>
								</ul>

								<!--
								//======================================================================//
								//																		//
								//				Import του κάθε Calendar σε κάθε Tab					//
								//																		//
								//======================================================================//
								-->

								<!-- Βάλε εδώ το fullcalendar για το 1ο έτος -->
								<div id="etos_ena">
									<table width="1165px">
										<tbody>
											<tr>
												<!-- <td align="center" style="width:1165px;">
													<div class="active_calendar">
														<p title="Για την σωστή λειτουργία της εφαρμογής 
														καταχωρήστε πρώτα το σωστό ενεργό ημερολόγιο 
														και προσπαθήστε να το αλλάζετε όσο το δυνατόν σπανιότερα." style="font-size:15px;">Ενεργό ημερολόγιο: <?php echo $active_calendar_start." έως ".$active_calendar_end;?> <span style="font-size:12px; color: #ddd; cursor: pointer;" onClick="changeActiveCalendar();" id="etos_ena_active_calendar"><u>Αλλαγή</u></span></p>
													<div>
												</td>-->
											</tr> 
											<tr>
												<td style="width:950; height:100%;">
													<div  id='calendar_ena' style="margin-top:30px;"></div> 	
												</td>
												<td style="width:215;">
													<div > 
														<div style="margin-top:30px;" align="center">
															<form action="fullcalendar.php#etos_ena" method="post">
																<input type="hidden" name="check" value="true"/>
																<input type="hidden" name="tab" value="0"/>
																<button style="height:23px;" type="submit" value="Έλεγχος Περιορισμών">Έλεγχος Περιορισμών</button>
															</form>
															
														</div>
														
													    <div id="accordion_ena" style="height:520; overflow: auto; padding-right: 1px;">
														  
														  <?php 
														  	if(isset($_POST['check']) && $_POST['check']==true && isset($_POST['tab']) && $_POST['tab']==0 ){
														  		include("errors/check_etos_ena.php"); 
														  	}
														  ?>
														  
														</div>
											 		</div>
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<a style="margin-top:-12px;" href="schedule-print.php" target="_blank"><img align="right" width="22px" height="22px" src="styles/basic/img/printer.png"></a>
												</td>
											</tr>
										</tbody>
									</table>	
								</div>
								

								<!-- Βάλε εδώ το fullcalendar για το 2ο έτος -->
								<div id="etos_dio">
									<table width="1165px">
										<tbody>
											<!-- <tr>
												<td align="center" style="width:1165px;">
													<div class="active_calendar">
														<p title="Για την σωστή λειτουργία της εφαρμογής 
														καταχωρήστε πρώτα το σωστό ενεργό ημερολόγιο 
														και προσπαθήστε να το αλλάζετε όσο το δυνατόν σπανιότερα." style="font-size:15px;">Ενεργό ημερολόγιο: <?php echo $active_calendar_start." έως ".$active_calendar_end;?> <span style="font-size:12px; color: #ddd; cursor: pointer;" onClick="changeActiveCalendar();" id="etos_ena_active_calendar"><u>Αλλαγή</u></span></p>
													<div>
												</td>
											</tr> -->
											<tr>
												<td style="width:950; height:100%;">
													<div  id='calendar_dio' style="margin-top:30px;"></div> 	<!-- Βάλε εδώ το fullcalendar -->
												</td>
												<td style="width:215;">
													<div> 
														<div style="margin-top:30px;" align="center">
															<form action="fullcalendar.php#etos_dio" method="post">
																<input type="hidden" name="check" value="true"/>
																<input type="hidden" name="tab" value="1"/>
																<button style="height:23px;" type="submit" value="Έλεγχος Περιορισμών">Έλεγχος Περιορισμών</button>
															</form>
														</div>
														
													    <div id="accordion_dio" style="height:520; overflow: auto; padding-right: 1px;">
														  
														  <?php 
														  	if(isset($_POST['check']) && $_POST['check']==true && isset($_POST['tab']) && $_POST['tab']==1 ){
														  		include("errors/check_etos_dio.php"); 
														  	}
														  ?>
														  
														</div>
													</div> 
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<a style="margin-top:-12px;" href="schedule-print.php" target="_blank"><img align="right" width="22px" height="22px" src="styles/basic/img/printer.png"></a>
												</td>
											</tr>
										</tbody>
									</table>	
								</div>

								<!-- Βάλε εδώ το fullcalendar για το 3ο έτος -->
								<div id="etos_tria">
									<table width="1165px">
										<tbody>
											<tr>
												<!-- <td align="center" style="width:1165px;">
													<div class="active_calendar">
														<p title="Για την σωστή λειτουργία της εφαρμογής 
														καταχωρήστε πρώτα το σωστό ενεργό ημερολόγιο 
														και προσπαθήστε να το αλλάζετε όσο το δυνατόν σπανιότερα." style="font-size:15px;">Ενεργό ημερολόγιο: <?php echo $active_calendar_start." έως ".$active_calendar_end;?> <span style="font-size:12px; color: #ddd; cursor: pointer;" onClick="changeActiveCalendar();" id="etos_ena_active_calendar"><u>Αλλαγή</u></span></p>
													<div>
												</td> -->
											</tr>
											<tr>
												<td style="width:950; height:100%;">
													<div  id='calendar_tria' style="margin-top:30px;"></div> 	<!-- Βάλε εδώ το fullcalendar -->
												</td>
												<td style="width:215;">
													<div> 
														<div style="margin-top:30px;" align="center">
															<form action="fullcalendar.php#etos_tria" method="post">
																<input type="hidden" name="check" value="true"/>
																<input type="hidden" name="tab" value="2"/>
																<button style="height:23px;" type="submit" value="Έλεγχος Περιορισμών">Έλεγχος Περιορισμών</button>
															</form>
														</div>
														
													    <div id="accordion_tria" style="height:520; overflow: auto; padding-right: 1px;">
														  
														  <?php 
														  	if(isset($_POST['check']) && $_POST['check']==true && isset($_POST['tab']) && $_POST['tab']==2 ){
														  		include("errors/check_etos_tria.php"); 
														  	}
														  ?>
														  
														</div>
													</div> 
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<a style="margin-top:-12px;" href="schedule-print.php" target="_blank"><img align="right" width="22px" height="22px" src="styles/basic/img/printer.png"></a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>

								<!-- Βάλε εδώ το fullcalendar για το 4ο έτος -->
								<div id="etos_tessera">
									<table width="1165px">
										<tbody>
											<!-- <tr>
												<td align="center" style="width:1165px;">
													<div class="active_calendar">
														<p title="Για την σωστή λειτουργία της εφαρμογής 
														καταχωρήστε πρώτα το σωστό ενεργό ημερολόγιο 
														και προσπαθήστε να το αλλάζετε όσο το δυνατόν σπανιότερα." style="font-size:15px;">Ενεργό ημερολόγιο: <?php echo $active_calendar_start." έως ".$active_calendar_end;?> <span style="font-size:12px; color: #ddd; cursor: pointer;" onClick="changeActiveCalendar();" id="etos_ena_active_calendar"><u>Αλλαγή</u></span></p>
													<div>
												</td>
											</tr> -->
											<tr>
												<td style="width:950; height:100%;">
													<div  id='calendar_tessera' style="margin-top:30px;"></div> 	<!-- Βάλε εδώ το fullcalendar -->
												</td>
												<td style="width:215;">
													<div>  
														<div style="margin-top:30px;" align="center">
															<form action="fullcalendar.php#etos_tessera" method="post">
																<input type="hidden" name="check" value="true"/>
																<input type="hidden" name="tab" value="3"/>
																<button style="height:23px;" type="submit" value="Έλεγχος Περιορισμών">Έλεγχος Περιορισμών</button>
															</form>
														</div>
														
													    <div id="accordion_tessera" style="height:520; overflow: auto; padding-right: 1px;">
														  <?php 
															if(isset($_POST['check']) && $_POST['check']==true && isset($_POST['tab']) && $_POST['tab']==3 ){
														  		include("errors/check_etos_tessera.php"); 
														  	}
														  ?>
														  
														</div>
													</div>  
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<a style="margin-top:-12px;" href="schedule-print.php" target="_blank"><img align="right" width="22px" height="22px" src="styles/basic/img/printer.png"></a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>

								<!-- Βάλε εδώ το fullcalendar για το 5ο έτος -->
								<div id="etos_pente">
									<table width="1165px">
										<tbody>
											<!-- <tr>
												<td align="center" style="width:1165px;">
													<div class="active_calendar">
														<p title="Για την σωστή λειτουργία της εφαρμογής 
														καταχωρήστε πρώτα το σωστό ενεργό ημερολόγιο 
														και προσπαθήστε να το αλλάζετε όσο το δυνατόν σπανιότερα." style="font-size:15px;">Ενεργό ημερολόγιο: <?php echo $active_calendar_start." έως ".$active_calendar_end;?> <span style="font-size:12px; color: #ddd; cursor: pointer;" onClick="changeActiveCalendar();" id="etos_ena_active_calendar"><u>Αλλαγή</u></span></p>
													<div>
												</td>
											</tr> -->
											<tr>
												<td style="width:950; height:100%;">
													<div  id='calendar_pente' style="margin-top:30px;"></div> 	<!-- Βάλε εδώ το fullcalendar -->
												</td>
												<td style="width:215;">
													<div> 
														<div style="margin-top:30px;" align="center">
															<form action="fullcalendar.php#etos_pente" method="post">
																<input type="hidden" name="check" value="true"/>
																<input type="hidden" name="tab" value="4"/>
																<button style="height:23px;" type="submit" value="Έλεγχος Περιορισμών">Έλεγχος Περιορισμών</button>
															</form>
														</div>
														
													    <div id="accordion_pente" style="height:520; overflow: auto; padding-right: 1px;">
														  
														  <?php 
														  	if(isset($_POST['check']) && $_POST['check']==true && isset($_POST['tab']) && $_POST['tab']==4 ){
														  		include("errors/check_etos_pente.php"); 
														  	}
														  ?>
														  
														</div>
													</div> 
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<a style="margin-top:-12px;" href="schedule-print.php" target="_blank"><img align="right" width="22px" height="22px" src="styles/basic/img/printer.png"></a>
												</td>
											</tr>
										</tbody>
									</table>
								</div>

								
								<div id="etos_synoliko">
									<table width="1165px">
										<tbody>
											<!-- <tr>
												<td align="center" style="width:1165px;">
													<div class="active_calendar">
														<p title="Για την σωστή λειτουργία της εφαρμογής 
														καταχωρήστε πρώτα το σωστό ενεργό ημερολόγιο 
														και προσπαθήστε να το αλλάζετε όσο το δυνατόν σπανιότερα." style="font-size:15px;">Ενεργό ημερολόγιο: <?php echo $active_calendar_start." έως ".$active_calendar_end;?> <span style="font-size:12px; color: #ddd; cursor: pointer;" onClick="changeActiveCalendar();" id="etos_ena_active_calendar"><u>Αλλαγή</u></span></p>
													<div>
												</td>
											</tr> -->
											<tr>
												<td style="width:950; height:100%;">
													<div  id='calendar' style="margin-top:30px;"></div> 	<!-- Βάλε εδώ το fullcalendar -->
												</td>
												<td style="width:215;">
													<div> 
														<div style="margin-top:30px;" align="center">
															<form action="fullcalendar.php#etos_synoliko" method="post">
																<input type="hidden" name="check" value="true"/>
																<input type="hidden" name="tab" value="5"/>
																<button style="height:23px;" type="submit" value="Έλεγχος Περιορισμών">Έλεγχος Περιορισμών</button>
															</form>
														</div>
														
													    <div id="accordion_synoliko" style="height:520; overflow: auto; padding-right: 1px;">
														  
														  <?php 
														  	if(isset($_POST['check']) && $_POST['check']==true && isset($_POST['tab']) && $_POST['tab']==5 ){
														  		include("errors/check_etos_synoliko.php"); 
														  	}
														  ?>
														  
														</div>
													</div> 
												</td>
											</tr>
											<tr>
												<td colspan="2">
													<a style="margin-top:-12px;" href="schedule-print.php" target="_blank"><img align="right" width="22px" height="22px" src="styles/basic/img/printer.png"></a>
												</td>
											</tr>
										</tbody>
									</table>	
								</div>

								<!--
								//======================================================================//
								//																		//
								//					  Overlay για το loading effect						//
								//																		//
								//======================================================================//
								-->
								
								<div id="modal" class="modal"></div>

							</div>

						</center>			
					</td>
				</tr>
			</tbody>
		</table>
	</center>
</body>

</html>


<script>

	var availableTags = new Array(); // Φτιάξε έναν πίνακα availableTags 
	availableTags = <?php echo json_encode( $data ); ?>	// Κάνε echo τον πίνακα data 

	var availableTags_ena = new Array(); 
	availableTags_ena = <?php echo json_encode($data_ena); ?>

	var availableTags_dio = new Array(); 
	availableTags_dio = <?php echo json_encode($data_dio); ?>

	var availableTags_tria = new Array(); 
	availableTags_tria = <?php echo json_encode($data_tria); ?>

	var availableTags_tessera = new Array(); 
	availableTags_tessera = <?php echo json_encode($data_tessera); ?>

	var availableTags_pente = new Array(); 
	availableTags_pente = <?php echo json_encode($data_pente); ?>

	var availableTags_aithousa = new Array(); 
	availableTags_aithousa = <?php echo json_encode($data_aithousa); ?>
	
</script>

<script>
		var mathimataTags = new Array();	
		mathimataTags = <?php echo json_encode($data_ena); ?>
		
</script>
<?php

	if(isset($_POST['check']) && $_POST['check']==true){
		$toCheck = true;
		$tab = $_POST['tab'];
		include("includes/error_check.php");
		
		if($tab==1) {
?>
			<script>
				mathimataTags = <?php echo json_encode($data); ?>
			</script>
<?php
		}
		else if($tab==2) {
?>
			<script>
				mathimataTags = <?php echo json_encode($data_tria); ?>
			</script>
<?php
		}
		else if($tab==3) {
?>
			<script>
				mathimataTags = <?php echo json_encode($data_tessera); ?>
			</script>
<?php
		}
		else if($tab==4) {
?>
			<script>
				mathimataTags = <?php echo json_encode($data_pente); ?>
			</script>
<?php
		}
		else if($tab==5) {
?>
			<script>
				mathimataTags = <?php echo json_encode( $data ); ?>
			</script>
<?php
		}		
		
	}
?>