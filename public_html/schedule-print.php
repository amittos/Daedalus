
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" lang="el" xml:lang="en"> 

<head>
	<!--
	//======================================================================//
	//																		//
	//								Libraries								//
	//																		//
	//======================================================================//
	-->

	<title> Εβδομαδιαίο Πρόγραμμα - Δαίδαλος </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="Content-Language" content="el">

	<link rel="stylesheet" href="styles/basic/style.css"> <!-- New CSS of Doom for the Header -->
	<link rel="shortcut icon" href="https://pithos.grnet.gr/pithos/rest/icsd08158@aegean.gr/files/favicon.icon" />
	
	<link rel="stylesheet" href="styles/basic/jquery-ui.css"> <!-- JQuery βιβλιοθήκη, με πειραγμένα τα Tabs -->
	<script src="//code.jquery.com/jquery-1.10.2.min.js"></script>
	<script src="//code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>

	<script type="text/javascript" src="jspdf/jspdf.js"></script>
	<script type="text/javascript" src="jspdf/libs/Deflate/adler32cs.js"></script>
    <script type="text/javascript" src="jspdf/libs/FileSaver.js/FileSaver.js"></script>
    <script type="text/javascript" src="jspdf/libs/Blob.js/BlobBuilder.js"></script>
    <script type="text/javascript" src="jspdf/jspdf.plugin.addimage.js"></script>
    <script type="text/javascript" src="jspdf/jspdf.plugin.standard_fonts_metrics.js"></script>
    <script type="text/javascript" src="jspdf/jspdf.plugin.split_text_to_size.js"></script>
    <script type="text/javascript" src="jspdf/jspdf.plugin.from_html.js"></script>

	<!-- FullCalendar stylesheet and FullCalendar JS file -->
	<link rel='stylesheet' type='text/css' href='fullcalendar.css' />
	<script type='text/javascript' src='fullcalendar.js'></script>
	
	<!-- DataTables CSS -->
	<link rel="stylesheet" type="text/css" href="jquery.dataTables.css">  <!-- Local Installation -->
	<!-- DataTables -->
	<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>

	<script>
		var isReady_ena = 0;
		var isReady_dio = 0;
		var isReady_tria = 0;
		var isReady_tessera = 0;
		var isReady_pente = 0;
		var isReady_synoliko = 0;

		function printPage(){
			if(isReady_synoliko==1 && isReady_ena==1 && isReady_dio==1 && isReady_tria==1 && isReady_tessera==1 && isReady_pente==1){

				$("body").removeClass("loading");

			   	window.print(); 

			}
		}

	</script>

	<style>
 	#etos_ena    { page-break-inside:avoid; page-break-after:auto }
 	#etos_dio    { page-break-inside:avoid; page-break-after:auto }
 	#etos_tria    { page-break-inside:avoid; page-break-after:auto }
 	#etos_tessera    { page-break-inside:avoid; page-break-after:auto }
 	#etos_pente    { page-break-inside:avoid; page-break-after:auto }
 	#etos_synoliko    { page-break-inside:avoid; page-break-after:auto }
 	#classrooms		{ page-break-inside:avoid; page-break-after:auto }
	.fc-agenda-slots td div {
         height: 60px !important;
    }
 	.modal {
		    display:    none;
		    position:   fixed;
		    z-index:    1000;
		    top:        0;
		    left:       0;
		    height:     100%;
		    width:      100%;
		    background: rgba( 255, 255, 255, .8 ) 
		                url('img/ajax-loader.gif') 
		                50% 50% 
		                no-repeat;
		}

		/* When the body has the loading class, we turn
		   the scrollbar off with overflow:hidden */
		body.loading {
		    overflow: hidden;   
		}

		/* Anytime the body has the loading class, our
		   modal element will be visible */
		body.loading .modal {
		    display: block;
		}
     	.fc-today{
     		background: white;
     	}

	</style>
</head>
<body>
	<?php
		
  		include("eti/print/etos_synoliko_print.php");
  		include("eti/print/etos_ena_print.php");
  		include("eti/print/etos_dio_print.php");
  		include("eti/print/etos_tria_print.php");
  		include("eti/print/etos_tessera_print.php");
  		include("eti/print/etos_pente_print.php");
  		
  	?>

  	<div id="etos_ena">
		<table width="1365px">
			<tbody>
				<tr>

				</tr>
				<tr>
					<td style="width:950; height:100%;">
						<p style="text-align: center; font-size:25px;"></br><b>A' Εξάμηνο</b></p>
						<div  id='calendar_ena' style="margin-top:30px;"></div> 	
					</td>				
				</tr>
			</tbody>
		</table>	
	</div>
	

	<!-- Βάλε εδώ το fullcalendar για το 2ο έτος -->
	<div id="etos_dio">
		<table width="1365px">
			<tbody>
				<tr>
					<td style="width:950; height:100%;">
						<p style="text-align: center; font-size:25px;"></br><b>Γ' Εξάμηνο</b></p>
						<div  id='calendar_dio' style="margin-top:30px;"></div> 	<!-- Βάλε εδώ το fullcalendar -->
					</td>
				</tr>
			</tbody>
		</table>	
	</div>

	<!-- Βάλε εδώ το fullcalendar για το 3ο έτος -->
	<div id="etos_tria">
		<table width="1365px">
			<tbody>
				<tr>
					<td style="width:950; height:100%;">
						<p style="text-align: center; font-size:25px;"></br><b>Ε' Εξάμηνο</b></p>
						<div  id='calendar_tria' style="margin-top:30px;"></div> 	<!-- Βάλε εδώ το fullcalendar -->
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<!-- Βάλε εδώ το fullcalendar για το 4ο έτος -->
	<div id="etos_tessera">
		<table width="1365px">
			<tbody>
				<tr>
					<td style="width:950; height:100%;">
						<p style="text-align: center; font-size:25px;"></br><b>Ζ' Εξάμηνο</b></p>
						<div  id='calendar_tessera' style="margin-top:30px;"></div> 	<!-- Βάλε εδώ το fullcalendar -->
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<!-- Βάλε εδώ το fullcalendar για το 5ο έτος -->
	<div id="etos_pente">
		<table width="1365px">
			<tbody>
				<tr>
					<td style="width:950; height:100%;">
						<p style="text-align: center; font-size:25px;"></br><b>Θ' Εξάμηνο</b></p>
						<div  id='calendar_pente' style="margin-top:30px;"></div> 	<!-- Βάλε εδώ το fullcalendar -->
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div id="etos_synoliko">
		<table width="1365px">
			<tbody>
				<tr>
					<td style="width:950; height: 100%;">
						<p style="text-align: center; font-size:25px;"></br><b>Συκεντρωτικό πρόγραμμα</b></p>
						<div  id='calendar' style="margin-top:30px;"></div> 	<!-- Βάλε εδώ το fullcalendar -->
					</td>
				</tr>
			</tbody>
		</table>	
	</div>
	
	<div id="classrooms">
		<table width="1303px">
			<th colspan="3" style="text-align: left;">
				</br>ΧΩΡΟΙ ΔΙΔΑΣΚΑΛΙΑΣ ΘΕΩΡΗΤΙΚΩΝ ΚΑΙ ΕΡΓΑΣΤΗΡΙΑΚΩΝ ΜΑΘΗΜΑΤΩΝ</br></br>
			</th>
			<tr>
				<td><b>Αίθουσα</b></td> 
				<td><b>Κων/νος Σοφούλης</b></td> 
				<td>Αίθουσα Εκδηλώσεων Κτηρίου Προβατάρη (λιμάνι)</td>
			</tr>

			<tr>
				<td><b>Αίθουσα</b></td> 
				<td><b>Νο.1</b></td> 
				<td>Αίθουσα Σχολικού Συγκροτήματος (Ισόγειο, δεξιά)</td>
			</tr>
			<tr>
				<td><b>Αίθουσα</b></td> 
				<td><b>Νο.3</b></td> 
				<td>Αίθουσα Σχολικού Συγκροτήματος (Α’ όροφος, δεξιά και μετά δεξιά)</td>
			</tr>

			<tr>
				<td><b>Αίθουσα</b></td> 
				<td><b>ΔΗΜΗΤΡΑ</b></td> 
				<td>Αίθουσα Σχολικού Συγκροτήματος (Α’ όροφος, αριστερά και μετά αριστερά)</td>
			</tr>

			<tr>
				<td><b>Αίθουσα</b></td> 
				<td><b>ΛΗΤΩ</b></td> 
				<td>Αίθουσα Σχολικού Συγκροτήματος (Α’ όροφος, αριστερά και στο βάθος δεξιά)</td>
			</tr>

			<tr>		
				<td><b>Αίθουσα</b></td> 
				<td><b>ΧΑΡΙΤΙΝΗ</b></td> 
				<td>Αίθουσα Σχολικού Συγκροτήματος (Α’ όροφος, αριστερά και στο βάθος αριστερά)</td>
			</tr>

			<tr>		
				<td><b>Αίθουσα</b></td> 
				<td><b>ΜΥΡΤΩ</b></td> 
				<td>Αίθουσα Κτηρίου Λυμπέρη (Α’ όροφος)</td>
			</tr>

			<tr>		
				<td><b>Αίθουσα</b></td> 
				<td><b>ΔΟΡΥΣΣΑ</b></td> 
				<td>Αίθουσα Κτηρίου Λυμπέρη (είσοδος από οδό Γοργύρας, ισόγειο, είσοδος δεξιά)</td>
			</tr>

			<tr>		
				<td><b>Αίθουσα</b></td> 
				<td><b>ΑΡΤΕΜΙΣ</b></td> 
				<td>Αίθουσα Κτηρίου Λυμπέρη (είσοδος από οδό Γοργύρας, ισόγειο, είσοδος αριστερά)</td>
			</tr>

			<tr>		
				<td><b>Αίθουσα</b></td> 
				<td><b>ΛΥΔΙΑ</b></td> 
				<td>Αίθουσα Κτηρίου Λυμπέρη (είσοδος από οδό Γοργύρας, ημιώροφος)</td>
			</tr>	

			<tr>
				<td><b>Αίθουσα</b></td> 
				<td><b>Ν. Σχολικό</b></td> 
				<td>Αίθουσα Εκδηλώσεων Νέου Σχολικού Συγκροτήματος</td>
			</tr>

			<tr>		
				<td><b>Εργαστήριο</b></td> 
				<td><b>ΗΛΕΚΤΡΑ</b></td> 
				<td>Εργαστήριο Κτηρίου Λυμπέρη (Ισόγειο, αριστερά της κεντρικής εισόδου του κτιρίου)</td>
				
			</tr>

			<tr>
				<td><b>Εργαστήριο</b></td> 
				<td><b>ΑΛΚΜΗΝΗ</b></td> 
				<td>Εργαστήριο Κτηρίου Λυμπέρη (Ισόγειο, δεξιά της κεντρικής εισόδου του κτιρίου)</td>
			</tr>

			<tr>
				<td><b>Εργαστήριο</b></td>  
				<td><b>ΦΑΙΔΡΑ</b></td>  
				<td>Εργαστήριο Κτηρίου Λυμπέρη (Ισόγειο, δεύτερη είσοδος δεξιότερα της κεντρικής εισόδου του κτιρίου)</td> 
			</tr>
			
			<!--
			//======================================================================//
			//																		//
			//					  Overlay για το loading effect						//
			//																		//
			//======================================================================//
			-->
			
			<div id="modal" class="modal"></div>
	</div>

</body>