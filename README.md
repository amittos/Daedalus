# Daedalus

Daedalus is the undergraduate thesis project of Kyriakos Chatzikyriakou and Alexandros Mittos.

Daedalus is a web service made to serve the needs of the academic schedule creator in the department of Information and Communications Systems Engineering as it is a platform of digitation and partial automation of the academic schedule creation procedure. 

The huge amount of variables and the constant feedback needed so far for the creation of the academic schedule made the procedure time-consuming and laborious. In the meantime, the trend of times require the digitation of key supportive processes in an organization. 

After excessive survey we decided that the best approach to solving this particular problem is a web service with emphasis on an easy-to-use interface and fast data entry. The presence of such environment, not only solves the constrain satisfaction problem that is the creation of the academic schedule but sets the stage for a series of additional services for the current organization. 

The result was successful as the service was created and is functional. However, like any other software, so Daedalus is a work in progress as certain points need further improvements and/or additions. 

You can view a demo of this web service under http://icsdweb.aegean.gr/project/daedalus/fullcalendar.php
